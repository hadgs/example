<?php

namespace App\Services;

use Swift_Mailer;

class MailerService
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(\Swift_Message $message)
    {
        $message
            ->setFrom('*@*', 'Martin Janata')
            ->setSender('*@*', 'Martin Janata')
        ;
        $this->mailer->send($message);
    }

    public function sendSimpleMessage(string $subject, $recipients, string $body)
    {
        $message = (new \Swift_Message($subject))
            ->setTo($recipients)
            ->setBody($body, 'text/plain')
        ;

        $this->send($message);
    }
}
