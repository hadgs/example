<?php

namespace App\Services;

use App\Entity\Task;
use Google_Client;
use Google_Service_Tasks;
use Google_Service_Tasks_Task;
use Google_Service_Tasks_TaskList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GoogleService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(TokenStorageInterface $tokenStorage, ContainerInterface $container)
    {
        $this->tokenStorage = $tokenStorage;
        $this->container = $container;
    }

    public function authorize()
    {
        $provider = new Google_Client();
        $provider->setAuthConfig($this->container->get('kernel')->getProjectDir().'/src/Resources/client_id.json');
//        $provider = $this->container->get('google_client');
//        $provider->setRedirectUri('http://localhost:8765/google');
        $provider->setRedirectUri($this->container->get('router')->generate('google_controlller', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $provider->setScopes([Google_Service_Tasks::TASKS]);

        if ($this->getUser()->getGoogleToken()) {
            $accessToken = json_decode($this->getUser()->getGoogleToken(), true);
            $provider->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($provider->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($provider->getRefreshToken()) {
                $provider->fetchAccessTokenWithRefreshToken($provider->getRefreshToken());
                $this->getUser()->setGoogleToken(json_encode($provider->getAccessToken()));
                $this->get('doctrine.orm.entity_manager')->flush();
            } else {
                $this->container->get('session')->getFlashBag()->add('warning', 'You are not logged to Google account, task will not be synchronized.');
            }
        }

        return $provider;
    }

    protected function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!\is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

    public function createTask(Task $task)
    {
        $client = $this->authorize();
        if (!$client->isAccessTokenExpired()) {
            $service = new Google_Service_Tasks($client);
            $tasklist = $this->getTasklist($service);
            $gTask = new Google_Service_Tasks_Task();
            $this->setParameters($gTask, $task);
            $result = $service->tasks->insert($tasklist->getId(), $gTask);
            $this->postInsert($service, $tasklist, $task, $result);
        }
    }

    public function updateTask(Task $task)
    {
        $client = $this->authorize();
        if (!$client->isAccessTokenExpired()) {
            $service = new Google_Service_Tasks($client);

            $tasklist = $this->getTasklist($service);
            if (!$task->getGoogleID()) {
                $gTask = new Google_Service_Tasks_Task();
            } else {
                $gTask = $service->tasks->get($tasklist->getId(), $task->getGoogleID());
            }
            $this->setParameters($gTask, $task);

            if (!$task->getGoogleID()) {
                $result = $service->tasks->insert($tasklist->getId(), $gTask);
            } else {
                $result = $service->tasks->update($tasklist->getId(), $task->getGoogleID(), $gTask);
            }
            $this->postInsert($service, $tasklist, $task, $result);
        }
    }

    public function deleteTask(Task $task)
    {
        $client = $this->authorize();
        if (!$client->isAccessTokenExpired()) {
            $service = new Google_Service_Tasks($client);
            $tasklist = $this->getTasklist($service);

            if ($task->getGoogleID()) {
                $service->tasks->delete($tasklist->getId(), $task->getGoogleID());
            }
        }
    }

    private function setParameters(Google_Service_Tasks_Task $gTask, Task $task)
    {
        $gTask->setTitle($task->getName());
        $gTask->setNotes(html_entity_decode(strip_tags($task->getNote())));
        $gTask->setStatus('needsAction');
        if ($task->getDateDue()) {
            $gTask->setDue($task->getDateDue()->format('Y-m-d').'T00:00:00.000Z');
        }
        if ($task->getDateDone()) {
            $gTask->setCompleted($task->getDateDone()->format('Y-m-d').'T00:00:00.000Z');
            $gTask->setStatus('completed');
        }
    }

    private function getTasklist(Google_Service_Tasks $service)
    {
        if (!$this->getUser()->getGoogleTaskList()) {
            $tasklist = new Google_Service_Tasks_TaskList();
            $tasklist->setTitle('Mladejov');
            $tasklist = $service->tasklists->insert($tasklist);
            $this->getUser()->setGoogleTaskList($tasklist->getId());
        } else {
            $tasklist = $service->tasklists->get($this->getUser()->getGoogleTaskList());
        }

        return $tasklist;
    }

    private function postInsert(Google_Service_Tasks $service, Google_Service_Tasks_TaskList $tasklist, Task $task, Google_Service_Tasks_Task $result)
    {
        $task->setGoogleID($result->getId());
        if ($task->getParent() && $task->getParent()->getGoogleID()) {
            $service->tasks->move($tasklist->getId(), $result->getId(), ['parent' => $task->getParent()->getGoogleID()]);
        } else {
            $service->tasks->move($tasklist->getId(), $result->getId(), ['parent' => null]);
        }
    }
}
