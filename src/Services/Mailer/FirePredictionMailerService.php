<?php

namespace App\Services\Mailer;

use App\Services\MailerService;

class FirePredictionMailerService
{
    /**
     * @var MailerService
     */
    private $mailer;

    public function __construct(MailerService $mailer)
    {
        $this->mailer = $mailer;
    }

    public function execute()
    {
        $report = 'Fire prediction Mailer Service start:'.PHP_EOL;

        $dateNow = new \DateTime();
        $dateTomorrov = clone $dateNow;
        $dateTomorrov->modify('+2 day');
        $dateForWeb = $dateTomorrov->format('dm');

        $url = 'http://portal.chmi.cz/files/portal/docs/meteo/ok/pozary/inp'.$dateForWeb.'.png';

        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }

        if ($exists) {
            $message = new \Swift_Message('Hlášení požárů');
            $message->setTo('*@*');
            $message->setBody('Hlášení požárů', 'text/plain');
            $attachment = \Swift_Attachment::fromPath($url);
            $message->attach($attachment);
            $this->mailer->send($message);
        } else {
            $report .= 'Fire prediction Mailer Service error.'.PHP_EOL;
        }

        $report .= 'Fire prediction Mailer Service end.'.PHP_EOL;

        return $report;
    }
}
