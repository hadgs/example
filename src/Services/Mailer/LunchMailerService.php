<?php

namespace App\Services\Mailer;

use App\DBAL\Types\AttendanceType;
use App\Repository\EventRepository;
use App\Services\MailerService;
use DateTime;

class LunchMailerService
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var MailerService
     */
    private $mailerService;

    public function __construct(EventRepository $eventRepository, MailerService $mailerService)
    {
        $this->eventRepository = $eventRepository;
        $this->mailerService = $mailerService;
    }

    public function execute()
    {
        $message = 'Lunch Mailer Service start:'.PHP_EOL;
        try {
            $date = new DateTime('+2 days');
            $events = $this->eventRepository->findByDate($date);
            $message .= sprintf('Found %d events.'.PHP_EOL, count($events));
            $users = [];
            $helpers = [];

            foreach ($events as $event) {
                foreach ($event->getAttendances() as $attendance) {
                    $presence = $attendance->getAttendance();
                    if ($presence == AttendanceType::YES) {
                        $users[$attendance->getUser()->getId()] = $attendance->getUser()->getFullName();
                        if ($attendance->getLunchCorrection() > 0 && (
                                !isset($helpers[$attendance->getUser()->getId()]) ||
                                $helpers[$attendance->getUser()->getId()] < $attendance->getLunchCorrection()
                            )
                        ) {
                            $helpers[$attendance->getUser()->getId()] = $attendance->getLunchCorrection();
                        }
                    } elseif ($presence == AttendanceType::MAYBE && ! isset($users[$attendance->getUser()->getId()])) {
                        $users[$attendance->getUser()->getId()] = $attendance->getUser()->getFullName().' (?)';
                        if ($attendance->getLunchCorrection() > 0 && (
                                !isset($helpers[$attendance->getUser()->getId()]) ||
                                $helpers[$attendance->getUser()->getId()] < $attendance->getLunchCorrection()
                            )
                        ) {
                            $helpers[$attendance->getUser()->getId()] = $attendance->getLunchCorrection();
                        }
                    }
                }
            }

            foreach ($users as $id => &$user) {
                if (isset($helpers[$id])) {
                    $user .= ' (+'.$helpers[$id].')';
                }
            }
            unset($user);
            sort($users);

            if ($users) {
                $recipients = ['*@*'];
                $subject = 'Mladějov přítomnost '.$date->format('d.m.Y');
                $body = 'Ahoj,'.PHP_EOL.PHP_EOL.'na den '.$date->format('d.m.Y').' je přihlášeno celkem '.(count($users)+array_sum($helpers)).' lidí, jmenovitě: '.PHP_EOL.implode(PHP_EOL, $users).PHP_EOL.PHP_EOL.'Martin (Tento email byl vygenerován automaticky.)';

                $this->mailerService->sendSimpleMessage($subject, $recipients, $body);
            }
            $message .= 'Lunch Mailer Service end.'.PHP_EOL;
        } catch (\Exception $e) {
            $message .= 'Lunch Mailer Service error: '.$e->getMessage().PHP_EOL;
        }

        return $message;
    }
}
