<?php

namespace App\Services;

use Psr\Log\LoggerInterface;

class LoggerService
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getLogger()
    {
        return $this->logger;
    }
}
