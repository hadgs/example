<?php

namespace App\Entity;

use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 *
 * @Vich\Uploadable
 */
class User extends BaseUser implements IdInterface
{
    use IdTrait;

    const FULLNAME = 'fullname';
    const GOOGLE_TOKEN = 'googleToken';
    const GOOGLE_TASK_LIST = 'googleTaskList';
    const IMAGE_FILE = 'imageFile';
    const LASTNAME = 'lastname';
    const LICENSE = 'license';
    const PROFILE_IMAGE = 'profileImage';
    const SHORTEDNAME = 'shortedname';
    const STUDENT = 'student';

    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\License", inversedBy="users")
     */
    protected $license;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Attendance", mappedBy="user", orphanRemoval=true)
     */
    protected $attendances;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $student;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profileImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="profileImage")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bill", mappedBy="requestingUser")
     */
    private $bills;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Task", mappedBy="responsibleUsers")
     */
    private $responsibleTasks;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $googleToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleTaskList;

    public function __construct()
    {
        parent::__construct();
        $this->license = new ArrayCollection();
        $this->attendances = new ArrayCollection();
        $this->bills = new ArrayCollection();
        $this->responsibleTasks = new ArrayCollection();
    }

    /**
     *
     * @return Collection|License[]
     */
    public function getLicense(): ?Collection
    {
        return $this->license;
    }

    public function addLicense(License $license): self
    {
        if (!$this->license->contains($license)) {
            $this->license[] = $license;
        }

        return $this;
    }

    public function removeLicense(License $license): self
    {
        if ($this->license->contains($license)) {
            $this->license->removeElement($license);
        }

        return $this;
    }

    /**
     *
     * @return Collection|Attendance[]
     */
    public function getAttendances(): Collection
    {
        return $this->attendances;
    }

    /**
     * @param $id
     * @return Attendance|null
     */
    public function getAttendancesByEventId($id)
    {
        foreach ($this->getAttendances() as $attendance) {
            if ($attendance->getEvent()->getId() === $id) {
                return $attendance;
            }
        }

        return null;
    }

    public function addAttendance(Attendance $attendance): self
    {
        if (!$this->attendances->contains($attendance)) {
            $this->attendances[] = $attendance;
            $attendance->setUser($this);
        }

        return $this;
    }

    public function removeAttendance(Attendance $attendance): self
    {
        if ($this->attendances->contains($attendance)) {
            $this->attendances->removeElement($attendance);
            // set the owning side to null (unless already changed)
            if ($attendance->getUser() === $this) {
                $attendance->setUser(null);
            }
        }

        return $this;
    }

    public function getStudent(): ?bool
    {
        return $this->student;
    }

    public function setStudent(bool $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getFullName(): string
    {
        return $this->getLastname().' '.$this->getFirstname();
    }

    public function getShortedName(): string
    {
        return $this->getLastname().' '.mb_substr($this->getFirstname(), 0, 1).'.';
    }

    public function getProfileImage(): ?string
    {
        return $this->profileImage;
    }

    public function setProfileImage(?string $profileImage): self
    {
        $this->profileImage = $profileImage;

        return $this;
    }
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return Collection|Bill[]
     */
    public function getBills(): Collection
    {
        return $this->bills;
    }

    public function addBill(Bill $bill): self
    {
        if (!$this->bills->contains($bill)) {
            $this->bills[] = $bill;
            $bill->setRequestingUser($this);
        }

        return $this;
    }

    public function removeBill(Bill $bill): self
    {
        if ($this->bills->contains($bill)) {
            $this->bills->removeElement($bill);
            // set the owning side to null (unless already changed)
            if ($bill->getRequestingUser() === $this) {
                $bill->setRequestingUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getResponsibleTasks(): Collection
    {
        return $this->responsibleTasks;
    }

    public function addResponsibleTasks(Task $responsibleTask): self
    {
        if (!$this->responsibleTasks->contains($responsibleTask)) {
            $this->responsibleTasks[] = $responsibleTask;
            $responsibleTask->addResponsibleUsers($this);
        }

        return $this;
    }

    public function removeResponsibleTasks(Task $responsibleTask): self
    {
        if ($this->responsibleTasks->contains($responsibleTask)) {
            $this->responsibleTasks->removeElement($responsibleTask);
            $responsibleTask->removeResponsibleUsers($this);
        }

        return $this;
    }

    public function getGoogleToken()
    {
        return $this->googleToken;
    }

    public function setGoogleToken($googleToken): self
    {
        $this->googleToken = $googleToken;

        return $this;
    }

    public function getGoogleTaskList(): ?string
    {
        return $this->googleTaskList;
    }

    public function setGoogleTaskList(?string $googleTaskList): self
    {
        $this->googleTaskList = $googleTaskList;

        return $this;
    }
}
