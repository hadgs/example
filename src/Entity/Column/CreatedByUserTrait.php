<?php

namespace App\Entity\Column;

use App\Entity\User;

trait CreatedByUserTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $createdByUser;

    public function getCreatedByUser(): ?User
    {
        return $this->createdByUser;
    }

    public function setCreatedByUser(?User $createdByUser): self
    {
        $this->createdByUser = $createdByUser;

        return $this;
    }
}
