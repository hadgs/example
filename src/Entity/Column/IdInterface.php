<?php

namespace App\Entity\Column;

interface IdInterface
{
    const ID = 'id';

    /**
     * @return int|null
     */
    public function getId();
}
