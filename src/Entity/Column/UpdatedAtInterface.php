<?php

namespace App\Entity\Column;

interface UpdatedAtInterface
{
    const UPDATED_AT = 'updatedAt';

    public function getUpdatedAt(): ?\DateTimeInterface;

    public function setUpdatedAt(?\DateTimeInterface $updatedAt);
}
