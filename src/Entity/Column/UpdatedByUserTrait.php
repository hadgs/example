<?php

namespace App\Entity\Column;

use App\Entity\User;

trait UpdatedByUserTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $updatedByUser;

    public function getUpdatedByUser(): ?User
    {
        return $this->updatedByUser;
    }

    public function setUpdatedByUser(?User $updatedByUser): self
    {
        $this->updatedByUser = $updatedByUser;

        return $this;
    }
}
