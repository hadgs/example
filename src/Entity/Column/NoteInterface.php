<?php

namespace App\Entity\Column;

interface NoteInterface
{
    const NOTE = 'note';

    public function getNote(): ?string;

    public function setNote(?string $note);
}
