<?php

namespace App\Entity\Column;

trait UpdatedAtTrait
{
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
