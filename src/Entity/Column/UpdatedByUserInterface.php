<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 17.11.18
 * Time: 17:05
 */

namespace App\Entity\Column;

use App\Entity\User;

interface UpdatedByUserInterface
{
    const UPDATED_BY_USER = 'updatedByUser';

    public function getUpdatedByUser(): ?User;

    public function setUpdatedByUser(?User $updatedByUser);
}
