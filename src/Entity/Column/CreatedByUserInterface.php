<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 17.11.18
 * Time: 17:05
 */

namespace App\Entity\Column;

use App\Entity\User;

interface CreatedByUserInterface
{
    const CREATED_BY_USER = 'createdByUser';

    public function getCreatedByUser(): ?User;

    public function setCreatedByUser(?User $createdByUser);
}
