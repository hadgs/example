<?php

namespace App\Entity\Column;

interface CreatedAtInterface
{
    const CREATED_AT = 'createdAt';

    public function getCreatedAt(): ?\DateTimeInterface;

    public function setCreatedAt(?\DateTimeInterface $createdAt);
}
