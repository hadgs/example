<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 17.11.18
 * Time: 12:47
 */

namespace App\Entity\Column;

interface NameInterface
{
    const NAME = 'name';

    public function getName(): ?string;

    public function setName(?string $name);
}
