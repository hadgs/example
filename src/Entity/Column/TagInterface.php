<?php

namespace App\Entity\Column;

use App\Entity\Tag;
use Doctrine\Common\Collections\Collection;

interface TagInterface
{
    const TAGS = 'tags';

    public function getTags(): Collection;

    public function addTag(Tag $tag);

    public function removeTag(Tag $tag);
}
