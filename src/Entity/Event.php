<?php

namespace App\Entity;

use App\Entity\Column\CreatedByUserInterface;
use App\Entity\Column\CreatedByUserTrait;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use App\Entity\Column\NoteInterface;
use App\Entity\Column\NoteTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event implements IdInterface, CreatedByUserInterface, NameInterface, NoteInterface
{
    use IdTrait;
    use CreatedByUserTrait;
    use NameTrait;
    use NoteTrait;

    const CAST = 'cast';
    const DATE = 'date';
    const TYPE = 'type';

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Attendance", mappedBy="event", orphanRemoval=true)
     */
    private $attendances;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventCast", mappedBy="event", orphanRemoval=true, cascade="all")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $cast;

    /**
     * @ORM\Column(type="EventCaseType")
     */
    private $type;

    private $dateEnd;

    private $dateInterval;

    public function __construct()
    {
        $this->attendances = new ArrayCollection();
        $this->cast = new ArrayCollection();
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     *
     * @return Collection|Attendance[]
     */
    public function getAttendances(): Collection
    {
        return $this->attendances;
    }

    public function addAttendance(Attendance $attendance): self
    {
        if (!$this->attendances->contains($attendance)) {
            $this->attendances[] = $attendance;
            $attendance->setEvent($this);
        }

        return $this;
    }

    public function removeAttendance(Attendance $attendance): self
    {
        if ($this->attendances->contains($attendance)) {
            $this->attendances->removeElement($attendance);
            // set the owning side to null (unless already changed)
            if ($attendance->getEvent() === $this) {
                $attendance->setEvent(null);
            }
        }

        return $this;
    }

    /**
     *
     * @return Collection|EventCast[]
     */
    public function getCast(): Collection
    {
        return $this->cast;
    }

    public function setCast(Collection $cast): self
    {
        $this->cast = $cast;
        foreach ($cast as $c) {
            $c->setEvent($this);
        }

        return $this;
    }

    public function addCast(EventCast $cast): self
    {
        if (!$this->cast->contains($cast)) {
            $this->cast[] = $cast;
            $cast->setEvent($this);
        }

        return $this;
    }

    public function removeCast(EventCast $cast): self
    {
        if ($this->cast->contains($cast)) {
            $this->cast->removeElement($cast);
            // set the owning side to null (unless already changed)
            if ($cast->getEvent() === $this) {
                $cast->setEvent(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getId() ? $this->getName().' '.$this->getDate()->format('d.m.Y') : 'Akce';
    }

    public function __clone()
    {
        $castClone = new ArrayCollection();
        foreach ($this->cast as $cast) {
            $itemClone = clone $cast;
            $itemClone->setEvent($this);
            $castClone->add($itemClone);
        }
        $this->cast = $castClone;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getDateInterval(): ?int
    {
        return $this->dateInterval;
    }

    public function setDateInterval(?int $dateInterval): self
    {
        $this->dateInterval = $dateInterval;

        return $this;
    }
}
