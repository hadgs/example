<?php

namespace App\Entity;

use App\Entity\Attendance;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NoteInterface;
use App\Entity\Column\NoteTrait;
use App\Entity\Event;
use App\Entity\License;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\EventCastRepository")
 */
class EventCast implements IdInterface, NoteInterface
{
    use IdTrait;
    use NoteTrait;

    const ATTENDANCES = 'attendances';
    const EVENT = 'event';
    const LICENSE = 'license';
    const POSITION = 'position';

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\License", inversedBy="featured")
     */
    private $license;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Attendance", mappedBy="eventCasts")
     */
    private $attendances;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="cast")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    public function __construct()
    {
        $this->attendances = new ArrayCollection();
    }

    public function getLicense(): ?License
    {
        return $this->license;
    }

    public function setLicense(License $license): self
    {
        $this->license = $license;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     *
     * @return Collection|Attendance[]
     */
    public function getAttendances(): Collection
    {
        return $this->attendances;
    }

    public function addAttendance(Attendance $attendance): self
    {
        if (!$this->attendances->contains($attendance)) {
            $this->attendances[] = $attendance;
            $attendance->addEventCast($this);
        }

        return $this;
    }

    public function removeAttendance(Attendance $attendance): self
    {
        if ($this->attendances->contains($attendance)) {
            $this->attendances->removeElement($attendance);
            $attendance->removeEventCast($this);
        }

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function __toString()
    {
        return ($this->getAttendances()->isEmpty() ? '' : '*').$this->getLicense()->getName().($this->getNote() ? ' - '.$this->getNote() : '');
    }
}
