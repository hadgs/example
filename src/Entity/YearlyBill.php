<?php

namespace App\Entity;

use App\Entity\Column\CreatedAtInterface;
use App\Entity\Column\CreatedAtTrait;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\UpdatedAtInterface;
use App\Entity\Column\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\YearlyBillRepository")
 */
class YearlyBill implements
    IdInterface,
    CreatedAtInterface,
    UpdatedAtInterface
{
    use IdTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    const ACCOUNT = 'account';
    const AMOUNT = 'amount';
    const YEAR = 'year';

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="YearlyAccountType")
     */
    private $account;

    /**
     * @ORM\Column(type="date")
     */
    private $year;

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount($account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getYear(): ?\DateTimeInterface
    {
        return $this->year;
    }

    public function setYear(\DateTimeInterface $year): self
    {
        $this->year = $year;

        return $this;
    }
}
