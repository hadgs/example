<?php

namespace App\Entity\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use InvalidArgumentException;

abstract class AbstractEnumType extends AbstractType
{
    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $types = "'".implode("', '", static::getChoices())."'";

        return "ENUM(".$types.")";
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $this->convert($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $this->convert($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    private function convert($value)
    {
        if (empty($value)) {
            return null;
        } elseif (in_array($value, static::getChoices())) {
            return $value;
        } else {
            throw new InvalidArgumentException("Unknown value \"$value\" of ".__CLASS__);
        }
    }
}
