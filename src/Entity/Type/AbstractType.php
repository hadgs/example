<?php

namespace App\Entity\Type;

use Doctrine\DBAL\Types\Type;

abstract class AbstractType extends Type
{
    /** @var string */
    protected $name = '';

    /**
     * @var array Array of ENUM Values, where ENUM values are keys and their readable versions are values
     *
     * @static
     */
    protected static $choices = [];

    /**
     * Get readable choices for the ENUM field.
     *
     * @static
     *
     * @return array Values for the ENUM field
     */
    public static function getChoices(): array
    {
        return \array_flip(static::$choices);
    }

    /**
     * Get array of ENUM Values, where ENUM values are keys and their readable versions are values.
     *
     * @static
     *
     * @return array Array of values with readable format
     */
    public static function getReadableValues(): array
    {
        return static::$choices;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTitle(string $type): string
    {
        if (!isset(static::$choices[$type])) {
            throw new \InvalidArgumentException(\sprintf('Invalid value "%s" for ENUM type "%s".', $type, static::class));
        }

        return static::$choices[$type];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name ?: \array_search(\get_class($this), self::getTypesMap(), true);
    }
}
