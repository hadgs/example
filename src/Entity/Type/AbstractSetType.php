<?php

namespace App\Entity\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use InvalidArgumentException;

abstract class AbstractSetType extends AbstractType
{
    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $types = "'".implode("','", static::getChoices())."'";

        return "set($types)";
    }

    /**
     * @param string           $value
     * @param AbstractPlatform $platform
     * @return array
     * @throws InvalidArgumentException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return [];
        }
        $values = @explode(',', $value);
        if (is_array($values) && empty(array_diff($values, static::getChoices()))) {
            return $values;
        } else {
            throw new InvalidArgumentException('Unknown value(s) "'.var_export($value, true).'" for '.__CLASS__);
        }
    }

    /**
     * @param array            $value
     * @param AbstractPlatform $platform
     * @return string
     * @throws InvalidArgumentException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return '';
        }
        if (is_array($value) && empty(array_diff($value, static::getChoices()))) {
            return implode(',', $value);
        } else {
            throw new InvalidArgumentException('Unknown value(s) "'.var_export($value, true).'" for '.__CLASS__);
        }
    }
}
