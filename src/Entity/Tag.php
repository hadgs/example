<?php

namespace App\Entity;

use App\Entity\Column\CreatedAtInterface;
use App\Entity\Column\CreatedAtTrait;
use App\Entity\Column\CreatedByUserInterface;
use App\Entity\Column\CreatedByUserTrait;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag implements IdInterface, NameInterface, CreatedByUserInterface, CreatedAtInterface
{
    use CreatedAtTrait;
    use CreatedByUserTrait;
    use IdTrait;
    use NameTrait;

    const AVAILABLE = 'available';

    public static $classes = [
        Task::class => 0x01,
        Bill::class => 0x02,
    ];

    /**
     * @ORM\Column(type="integer")
     */
    private $available;

    public function getAvailable()
    {
        return $this->available;
    }

    public function setAvailable(int $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function isClassAvailable(string $class): bool
    {
        return (bool) ($this->available & static::$classes[$class]);
    }

    public function __toString()
    {
        return $this->getName() ?: '';
    }
}
