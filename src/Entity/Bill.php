<?php

namespace App\Entity;

use App\DBAL\Types\AccountType;
use App\DBAL\Types\BillDirectionType;
use App\DBAL\Types\BillStateType;
use App\Entity\Column\CreatedAtInterface;
use App\Entity\Column\CreatedAtTrait;
use App\Entity\Column\CreatedByUserInterface;
use App\Entity\Column\CreatedByUserTrait;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use App\Entity\Column\TagInterface;
use App\Entity\Column\TagTrait;
use App\Entity\Column\UpdatedAtInterface;
use App\Entity\Column\UpdatedAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillRepository")
 *
 * @Vich\Uploadable
 */
class Bill implements
    IdInterface,
    CreatedAtInterface,
    CreatedByUserInterface,
    NameInterface,
    TagInterface,
    UpdatedAtInterface
{
    use IdTrait;
    use CreatedAtTrait;
    use CreatedByUserTrait;
    use NameTrait;
    use TagTrait;
    use UpdatedAtTrait;

    const ACCOUNT = 'account';
    const ACCOUNT_TO = 'accountTo';
    const AMOUNT = 'amount';
    const CATEGORY = 'category';
    const DATE_PAID = 'datePaid';
    const NUMBER = 'number';
    const REQUESTING_USER = 'requestingUser';
    const STATE = 'state';
    const DOCUMENT_FILE = 'documentFile';
    const DOCUMENT_NAME = 'documentName';

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bills")
     */
    private $requestingUser;

    /**
     * @ORM\Column(type="AccountType")
     */
    private $account;

    /**
     * @ORM\Column(type="BillStateType")
     */
    private $state;

    /**
     * @ORM\Column(type="AccountType", nullable=true)
     */
    private $accountTo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datePaid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillCategory", inversedBy="bills")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $documentName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="bill_document", fileNameProperty="documentName")
     *
     * @var File
     */
    private $documentFile;

    public function __construct()
    {
        $this->accountTo = null;
        $this->state = BillStateType::FRESH;
        $this->tags = new ArrayCollection();
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param mixed                     $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getCategory() && $this->getCategory()->getDirection() === BillDirectionType::TRANSFER) {
            if ($this->getAccountTo() === null) {
                $context->buildViolation('Must be set when transfer.')
                    ->atPath(self::ACCOUNT_TO)
                    ->addViolation();
            }
            if ($this->getAccountTo() === $this->getAccount()) {
                $context->buildViolation('Must be same as account from.')
                    ->atPath(self::ACCOUNT_TO)
                    ->addViolation();
            }
        }
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function getAmountAsString(): string
    {
        return number_format($this->getAmount(), 2, ",", '');
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getRequestingUser(): ?User
    {
        return $this->requestingUser;
    }

    public function setRequestingUser(?User $requestingUser): self
    {
        $this->requestingUser = $requestingUser;

        return $this;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function getAccountAsString()
    {
        return AccountType::getReadableValue($this->account);
    }

    public function setAccount($account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getStateAsString()
    {
        return BillStateType::getReadableValue($this->state);
    }

    public function setState($state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDatePaid(): ?\DateTimeInterface
    {
        return $this->datePaid;
    }

    public function setDatePaid(?\DateTimeInterface $datePaid): self
    {
        $this->datePaid = $datePaid;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCategory(): ?BillCategory
    {
        return $this->category;
    }

    public function setCategory(?BillCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDocumentName(): ?string
    {
        return $this->documentName;
    }

    public function setDocumentName(?string $documentName): self
    {
        $this->documentName = $documentName;

        return $this;
    }
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setDocumentFile(?File $file = null): void
    {
        $this->documentFile = $file;

        if (null !== $file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdatedAt(new \DateTimeImmutable());
        }
    }

    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    /**
     * @return string|null
     */
    public function getAccountTo()
    {
        return $this->accountTo;
    }

    /**
     * @param $accountTo
     * @return $this
     */
    public function setAccountTo($accountTo): self
    {
        $this->accountTo = $accountTo;

        return $this;
    }
    
    public function __toString(): string
    {
        return $this->getName() != null ? $this->getName() : '';
    }
}
