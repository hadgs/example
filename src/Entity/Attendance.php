<?php

namespace App\Entity;

use App\DBAL\Types\AttendanceType;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NoteInterface;
use App\Entity\Column\NoteTrait;
use App\Entity\User;
use App\DBAL\Types\PaidType;
use App\Entity\Event;
use App\Entity\EventCast;
use App\Repository\AttendanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttendanceRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_event",columns={"user", "event"})})
 */
class Attendance implements IdInterface, NoteInterface
{
    use IdTrait;
    use NoteTrait;

    const USER = 'user';
    const EVENT = 'event';
    const EVENT_CASTS = 'eventCasts';
    const PAID = 'paid';
    const ATTENDANCE = 'attendance';
    const LUNCH_CORRECTION = 'lunchCorrection';

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="attendances")
     * @ORM\JoinColumn(name="user", nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EventCast", inversedBy="attendances")
     */
    private $eventCasts;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="attendances")
     * @ORM\JoinColumn(name="event", nullable=false)
     */
    private $event;

    /**
     *
     * @ORM\Column(type="PaidType")
     *
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\PaidType")
     */
    private $paid;

    /**
     *
     * @ORM\Column(type="AttendanceType")
     */
    private $attendance;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $lunchCorrection;

    public function __construct()
    {
        $this->eventCasts = new ArrayCollection();
        $this->attendance = AttendanceType::YES;
        $this->paid = PaidType::NOT_AVAILABLE;
        $this->lunchCorrection = 0;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param mixed                     $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (/*(!$this->attendance && $this->paid !== PaidType::NOT_AVAILABLE)
            || */($this->attendance && $this->getUser() && !$this->getUser()->getStudent() && $this->paid !== PaidType::NOT_AVAILABLE)
        ) {
            $context->buildViolation('Not possible')
                ->atPath(self::PAID)
                ->addViolation();
        }
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|EventCast[]
     */
    public function getEventCasts(): Collection
    {
        return $this->eventCasts;
    }

    public function addEventCast(EventCast $eventCast): self
    {
        if (!$this->eventCasts->contains($eventCast)) {
            $this->eventCasts[] = $eventCast;
        }

        return $this;
    }

    public function removeEventCast(EventCast $eventCast): self
    {
        if ($this->eventCasts->contains($eventCast)) {
            $this->eventCasts->removeElement($eventCast);
        }

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getPaid(): ?string
    {
        return $this->paid;
    }

    public function setPaid(string $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getAttendance(): ?string
    {
        return $this->attendance;
    }

    public function setAttendance(string $attendance): self
    {
        $this->attendance = $attendance;

        return $this;
    }

    public function getLunchCorrection(): int
    {
        return $this->lunchCorrection;
    }

    public function setLunchCorrection(int $lunchCorrection): self
    {
        $this->lunchCorrection = $lunchCorrection;

        return $this;
    }

    public function __toString()
    {
        return 'Účast '.($this->getEvent() ? $this->getEvent()->getName() : '');
    }
}
