<?php

namespace App\Entity;

use App\Entity\Column\CreatedAtInterface;
use App\Entity\Column\CreatedAtTrait;
use App\Entity\Column\CreatedByUserInterface;
use App\Entity\Column\CreatedByUserTrait;
use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use App\Entity\Column\NoteInterface;
use App\Entity\Column\NoteTrait;
use App\Entity\Column\TagInterface;
use App\Entity\Column\TagTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task implements
    IdInterface,
    CreatedAtInterface,
    CreatedByUserInterface,
    NameInterface,
    NoteInterface,
    TagInterface
{
    use IdTrait;
    use CreatedAtTrait;
    use CreatedByUserTrait;
    use NameTrait;
    use NoteTrait;
    use TagTrait;

    const DATE_DEADLINE = 'dateDeadline';
    const DATE_DONE = 'dateDone';
    const DATE_DUE = 'dateDue';
    const GOOGLE_ID = 'googleId';
    const PARENT = 'parent';
    const POSITION = 'position';
    const POSITION_CORRECTION = 'positionCorrection';
    const PRIORITY = 'priority';
    const RESPONSIBLE_USERS = 'responsibleUsers';
    const TASKS = 'tasks';
    const TIME_ESTIMATE = 'timeEstimate';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleID;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="responsibleTasks")
     */
    private $responsibleUsers;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDue;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDeadline;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="parent")
     */
    private $tasks;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="tasks")
     */
    private $parent;

    /**
     * @ORM\Column(type="PriorityType")
     */
    private $priority;

    /**
     * @ORM\Column(type="integer")
     */
    private $timeEstimate;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="integer")
     */
    private $positionCorrection;

    public function __construct()
    {
        $this->position = 0;
        $this->tags = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->responsibleUsers = new ArrayCollection();
    }

    public function getGoogleID(): ?string
    {
        return $this->googleID;
    }

    public function setGoogleID(?string $googleID): self
    {
        $this->googleID = $googleID;

        return $this;
    }

    public function getDateDue(): ?\DateTimeInterface
    {
        return $this->dateDue;
    }

    public function setDateDue(?\DateTimeInterface $dateDue): self
    {
        $this->dateDue = $dateDue;

        return $this;
    }

    public function getDateDeadline(): ?\DateTimeInterface
    {
        return $this->dateDeadline;
    }

    public function setDateDeadline(?\DateTimeInterface $dateDeadline): self
    {
        $this->dateDeadline = $dateDeadline;

        return $this;
    }

    public function getDateDone(): ?\DateTimeInterface
    {
        return $this->dateDone;
    }

    public function setDateDone(?\DateTimeInterface $dateDone): self
    {
        $this->dateDone = $dateDone;

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setParent($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getParent() === $this) {
                $task->setParent(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getTimeEstimate(): ?int
    {
        return $this->timeEstimate;
    }

    public function setTimeEstimate(int $timeEstimate): self
    {
        $this->timeEstimate = $timeEstimate;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getResponsibleUsers(): Collection
    {
        return $this->responsibleUsers;
    }

    public function addResponsibleUsers(User $responsibleUser): self
    {
        if (!$this->responsibleUsers->contains($responsibleUser)) {
            $this->responsibleUsers[] = $responsibleUser;
        }

        return $this;
    }

    public function removeResponsibleUsers(User $responsibleUser): self
    {
        if ($this->responsibleUsers->contains($responsibleUser)) {
            $this->responsibleUsers->removeElement($responsibleUser);
        }

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPositionCorrection(): ?int
    {
        return $this->positionCorrection;
    }

    public function setPositionCorrection($positionCorrection): self
    {
        $this->positionCorrection = $positionCorrection;

        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString()
    {
        return $this->getName() != null ? $this->getName() : '';
    }
}
