<?php

namespace App\Entity;

use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="fos_group")
 */
class Group extends BaseGroup implements IdInterface
{
    use IdTrait;

    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;
}
