<?php

namespace App\Entity;

use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillCategoryRepository")
 */
class BillCategory implements IdInterface, NameInterface
{
    use IdTrait;
    use NameTrait;

    /**
     * @ORM\Column(type="BillDirectionType")
     */
    private $direction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bill", mappedBy="category")
     */
    private $bills;

    public function __construct()
    {
        $this->bills = new ArrayCollection();
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function setDirection($direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return Collection|Bill[]
     */
    public function getBills(): Collection
    {
        return $this->bills;
    }

    public function addBill(Bill $bill): self
    {
        if (!$this->bills->contains($bill)) {
            $this->bills[] = $bill;
            $bill->setCategory($this);
        }

        return $this;
    }

    public function removeBill(Bill $bill): self
    {
        if ($this->bills->contains($bill)) {
            $this->bills->removeElement($bill);
            // set the owning side to null (unless already changed)
            if ($bill->getCategory() === $this) {
                $bill->setCategory(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name ?: '';
    }
}
