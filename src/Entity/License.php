<?php

namespace App\Entity;

use App\Entity\Column\IdInterface;
use App\Entity\Column\IdTrait;
use App\Entity\Column\NameInterface;
use App\Entity\Column\NameTrait;
use App\Entity\Column\NoteInterface;
use App\Entity\Column\NoteTrait;
use App\Entity\User;
use App\Entity\EventCast;
use App\Repository\LicenseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\LicenseRepository")
 */
class License implements IdInterface, NameInterface, NoteInterface
{
    use IdTrait;
    use NameTrait;
    use NoteTrait;

    const ABBREVIATION = 'abbreviation';
    const USERS = 'users';

    /**
     *
     * @ORM\Column(type="string", length=8)
     */
    private $abbreviation;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventCast", mappedBy="license")
     */
    private $featured;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="license")
     */
    private $users;

    public function __construct()
    {
        $this->setFeatured(new ArrayCollection());
        $this->users = new ArrayCollection();
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getFeatured()
    {
        return $this->featured;
    }

    public function setFeatured($featured): self
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     *
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addLicense($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeLicense($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName() ? $this->getName() : '';
    }
}
