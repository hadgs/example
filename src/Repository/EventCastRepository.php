<?php

namespace App\Repository;

use App\Entity\EventCast;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 *
 * @method EventCast|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventCast|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventCast[]    findAll()
 * @method EventCast[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventCastRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventCast::class);
    }

    public function getCastsForEvents(array $eventIds)
    {
        $qb = $this->createQueryBuilder('ec')
            ->leftJoin('ec.event', 'e')
            ->join('ec.license', 'l')
            ->select('e.id')
            ->addSelect('l.abbreviation');

        $dbResult = $qb
            ->andWhere('ec.event IN (:events)')
            ->setParameter('events', $eventIds)
            ->getQuery()
            ->getResult()
            ;
        $result = [];

        foreach ($dbResult as $oneCast) {
            $result[$oneCast['id']][] = $oneCast['abbreviation'];
        }

        return $result;
    }
    
//    /**
//     * @return Event[] Returns an array of EventCast objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('ec')
            ->andWhere('ec.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('ec.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventCast
    {
        return $this->createQueryBuilder('ec')
            ->andWhere('ec.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
