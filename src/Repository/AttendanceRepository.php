<?php

namespace App\Repository;

use App\Entity\Attendance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 *
 * @method Attendance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Attendance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Attendance[]    findAll()
 * @method Attendance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttendanceRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Attendance::class);
    }

    public function findByUsersAndEvents(array $usersIds, array $eventIds)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.user', 'u')
            ->join('a.event', 'e')
            ->leftJoin('a.eventCasts', 'ec')
            ->leftJoin('ec.license', 'l')
            ->select('a.id')
            ->addSelect('a.attendance')
            ->addSelect('a.note')
            ->addSelect('a.lunchCorrection')
            ->addSelect('u.id as uid')
            ->addSelect('e.id as eid')
            ->addSelect('l.abbreviation');
        return $qb
            ->andWhere('a.event IN (:events)')
            ->setParameter('events', $eventIds)
            ->andWhere('a.user IN (:users)')
            ->setParameter('users', $usersIds)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Attendance
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
