<?php

namespace App\Repository;

use App\Entity\BillCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BillCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillCategory[]    findAll()
 * @method BillCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BillCategory::class);
    }

//    /**
//     * @return BillCategory[] Returns an array of BillCategory objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BillCategory
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
