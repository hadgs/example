<?php

namespace App\Repository;

use App\Entity\Event;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 *
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function filterByDateGreaterThan($date = null, $type = null)
    {
        $date = $date ?: (new \DateTime());
        $qb = $this->createQueryBuilder('e')
            ->andWhere('e.'.Event::DATE.' >= :val')
            ->setParameter('val', $date)
            ->orderBy('e.'.Event::DATE.'', 'ASC');

        if ($type) {
            $qb
                ->andWhere('e.'.Event::TYPE.' = :type')
                ->setParameter('type', $type);
        }

        return $qb;
    }

    public function filterById($id = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->andWhere('e.'.Event::ID.' = :val')
            ->setParameter('val', $id);

        return $qb;
    }

    public function findByDateGreaterThan($date = null, $type = null, $limit = null)
    {
        $qb = $this->filterByDateGreaterThan($date, $type);

        return $qb
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Event[] Returns an array of Event objects
     */
    public function findByDate(DateTime $date)
    {
        $qb = $this->createQueryBuilder('e');

        return $qb->andWhere($qb->expr()->like('e.'.Event::DATE, ':val'))
            ->setParameter('val', '%'.$date->format('Y-m-d').'%')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
