<?php

namespace App\Repository;

use App\Entity\YearlyBill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method YearlyBill|null find($id, $lockMode = null, $lockVersion = null)
 * @method YearlyBill|null findOneBy(array $criteria, array $orderBy = null)
 * @method YearlyBill[]    findAll()
 * @method YearlyBill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YearlyBillRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, YearlyBill::class);
    }

    // /**
    //  * @return YearlyBill[] Returns an array of YearlyBill objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('y.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?YearlyBill
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
