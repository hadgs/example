<?php

namespace App\Admin;

use App\DBAL\Types\EventCaseType;
use App\Entity\Attendance;
use App\Entity\Event;
use App\Entity\EventCast;
use App\Entity\User;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RojAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'admin_app_roj';
    protected $baseRoutePattern = 'app/roj';
    protected $classnameLabel = 'Roj';
    protected $maxPerPage = 256;

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => User::LASTNAME
    ];

    protected $attendances = [];

    protected $functions = [];

    public function isGranted($name, $object = null)
    {
        $isGranted = parent::isGranted($name, $object);

        if ('LIST' === $name) {
            return true;
        }

        return $isGranted;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        foreach (EventCaseType::getChoices() as $key => $value) {
            $menu->addChild($key, ['uri' => $this->generateUrl('list', ['type' => $value, 'events' => 15])]);
        }
        $year = (new \DateTime())->format('Y');
        $type = $this->getRequest()->get('type');
        if (is_null($type)) {
            $today = new \DateTime();
            $summer = new \DateTime('March 31');
            $winter = new \DateTime('September 31');
            if ($today >= $summer && $today < $winter) {
                $type = 'publicRides';
            } else {
                $type = 'other';
            }
        }
        $params = ['type' => $type, 'year' => $year];
        $menu->addChild('Celý rok', ['uri' => $this->generateUrl('list', $params)]);
        if (in_array($type, [EventCaseType::BRIGADE_DAYS, EventCaseType::OTHER])) {
            $params = array_merge($params, ['fromMonth' => (new \DateTime())->modify('-2 month')->format('m')]);
        }
        $menu->addChild('K tisku', ['uri' => $this->generateUrl('print', $params)]);
    }

    protected function configureListFields(ListMapper $list)
    {
        $type = $this->request->query->get('type');
        $year = $this->request->query->get('year');
        if (is_null($type)) {
			$today = new \DateTime();
			$summer = new \DateTime('March 31');
			$winter = new \DateTime('September 31');
			if ($today >= $summer && $today < $winter) {
				$type = 'publicRides';
			} else {
				$type = 'other';
			}
		}
        $num = $this->request->query->get('events', null);
        $date = new \DateTime();
        if (! is_null($year)) {
            $date->setDate($year, 1, 1);
        } else {
            $date->modify('-3 days');
        }
        $events = $this->getRepository(Event::class)->findByDateGreaterThan($date, $type, $num);
        $eventIds = array_map(
            function ($item) {
                return $item->getId();
            },
            $events
        );
        $this->functions = $this->getRepository(EventCast::class)->getCastsForEvents($eventIds);

        $list->add($this->getUser() instanceof User ? User::FULLNAME : User::SHORTEDNAME, null, [
            'label' => '',
            'header_style' => 'width: 111px;',
            'header_class' => 'roj_table',
            'template' => 'CRUD/list_roj_user.html.twig',
        ]);

        foreach ($events as $i => $event) {
            $list->add("event_$i", null, [
                'label' => $event->getName().' '.$event->getDate()->format('d.m.Y'),
                'event_id' => $event->getId(),
                'template' => 'CRUD/list_roj.html.twig',
                'row_align' => 'center',
                'header_style' => 'text-align: center',
                'header_class' => 'roj_table',
            ]);
        }
    }

    public function getAttendances()
    {
        if (empty($this->attendances)) {
            $users = array_map(
                function ($item) {
                    return $item->getId();
                },
                $this->datagrid->getPager()->getResults()
            );
            $events = [];
            foreach ($this->datagrid->getColumns()->getElements() as $key => $column) {
                if ($key == 'fullname') {
                    continue;
                }
                $events[] = $column->getOption('event_id');
            }
            $at = $this->getRepository(Attendance::class)->findByUsersAndEvents($users, $events);
            $attendances = [];
            foreach ($at as $a) {
                // todo:
                $attendances[$a['uid']][$a['eid']]['id'] = $a['id'];
                $attendances[$a['uid']][$a['eid']]['attendance'] = $a['attendance'];
                $attendances[$a['uid']][$a['eid']]['note'] = $a['note'];
                $attendances[$a['uid']][$a['eid']]['lunchCorrection'] = $a['lunchCorrection'];
                if ($a['abbreviation']) {
                    $attendances[$a['uid']][$a['eid']]['abbreviation'][] = $a['abbreviation'];
                }
            }
            $this->attendances = $attendances;
        }

        return $this->attendances;
    }

    public function getFunctions()
    {
        return $this->functions;
    }

    public function getExportFields()
    {
        $fields = [$this->getUser() instanceof User ? User::FULLNAME : User::SHORTEDNAME];

        for ($i = 0; $i < 15; $i++) {
            $fields[] = 'event_'.$i;
        }

        return $fields;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept('list');
        $collection->add('print');
    }
}
