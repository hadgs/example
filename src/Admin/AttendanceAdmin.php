<?php
/**
 * AttendanceAdmin.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

namespace App\Admin;

use App\DBAL\Types\AttendanceType;
use App\DBAL\Types\PaidType;
use App\Entity\Attendance;
use App\Entity\Event;
use App\Entity\EventCast;
use App\Entity\License;
use App\Entity\User;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DateRangePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Symfony\Component\Validator\Constraints\Range;

class AttendanceAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => Attendance::EVENT,
    ];

    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            [
                'event__date' => [
                    'value' => [
                        'start' => date('d.m.Y'),
                    ],
                ],
            ],
            $this->datagridValues
        );

        return parent::getFilterParameters();
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->preSave($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->preSave($object);
    }

    public function create($object)
    {
        try {
            $object = parent::create($object);
        } catch (ModelManagerException $e) {
            $object = $this->getModelManager()->findOneBy(Attendance::class, [Attendance::EVENT => $object->getEvent(), Attendance::USER => $object->getUser()]);
        }

        return $object;
    }

    public function isGranted($name, $object = null)
    {
        $object = $object ? $object : $this->getSubject();
        $isGranted = parent::isGranted($name, $object);

//        if ($isGranted && ('EDIT' === $name)) {
//            return $object && ((
//                ($object->getUser() && $object->getUser()->getId() === $this->getUser()->getId()) ||
//                !$object->getUser()) ||
//                parent::isGranted('ALL')
//            );
//        }

        return $isGranted;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $userId = $this->getRequest()->get('userId');
        $eventId = $this->getRequest()->get('eventId');
        $date = new \DateTime();
        $date->modify('-2 month');
        $preferred = $this->getRepository(Event::class)->findByDateGreaterThan();
        $queryEvent = $eventId ? $this->getRepository(Event::class)->filterById($eventId) : $this->getRepository(Event::class)->filterByDateGreaterThan($date);
        $form->add(
            Attendance::EVENT,
            ModelType::class,
            [
                'btn_add' => false,
                'disabled' => $this->isCurrentRoute('edit'),
                'label' => 'Akce',
                'query' => $queryEvent,
                'preferred_choices' => function ($value, $key) use ($preferred) {
                    foreach ($preferred as $event) {
                        if ($value == $event->getId()) {
                            return true;
                        }
                    }

                    return false;
                },
            ]
        );
        $form->add(
            Attendance::USER,
            EntityType::class,
            [
                'label' => 'Osoba',
                'class' => User::class,
                'disabled' => $this->isCurrentRoute('edit'),
                'choice_label' => 'getFullName',
                'query_builder' => function (EntityRepository $er) use ($userId) {
                    $qb = $er->createQueryBuilder('e')
                        ->orderBy('e.'.User::LASTNAME, 'ASC');
                    if ($userId) {
                        $qb->andWhere('e.'.User::ID.' = :userId')->setParameter('userId', $userId);
                    }

                    return $qb;
                },
                'preferred_choices' => [$this->getUser()],
            ],
            [
                'admin_code' => 'admin.user',
            ]
        );
        $form->add(
            Attendance::ATTENDANCE,
            ChoiceFieldMaskType::class,
            [
                'label' => 'Přítomen',
                'expanded' => true,
                'choices' => AttendanceType::getChoices(),
                'map' => [
                    AttendanceType::YES => [Attendance::EVENT_CASTS, Attendance::LUNCH_CORRECTION],
                    AttendanceType::MAYBE => [Attendance::LUNCH_CORRECTION],
                ],
            ]
        );
        $form->add(
            Attendance::LUNCH_CORRECTION,
            NumberType::class,
            [
                'label' => 'Pomocníků',
                'sonata_help' => 'počítají se i do obědů',
                'constraints' => [
                    new Range(['min' => 0, 'max' => 5]),
                ],
            ]
        );
        $form->add(
            Attendance::EVENT_CASTS,
            ModelAutocompleteType::class,
            [
                'btn_add' => false,
                'label' => 'Funkce',
                'sonata_help' => '* znamená obsazenou pozici',
                'minimum_input_length' => 0,
                'multiple' => true,
                'property' => 'id',
                'required' => false,
                'template' => 'CRUD/modeltype_autocomplete_custom.html.twig',
                'url' => $this->getConfigurationPool()->getContainer()->get('router')->generate('app_api_eventcastonattendance'),
                'req_params' =>
                [
                    'userId' => '$("#'.$this->getUniqid().'_'.Attendance::USER.'").val()',
                    'eventId' => '$("#'.$this->getUniqid().'_'.Attendance::EVENT.'").val()',
                ],
            ]
        );
        if ($this->isCurrentRoute('edit')) {
            $subject = $this->getSubject();
            if ($subject->getUser()->getStudent() && $this->isGranted('ALL')) {
                $form->add(
                    Attendance::PAID,
                    ChoiceType::class,
                    [
                        'label' => 'Proplacena cesta',
                        'expanded' => true,
                        'choices' => PaidType::getChoices(),
                    ]
                );
            }
        } else {
            $form->add(
                Attendance::PAID,
                HiddenType::class,
                ['data' => PaidType::NOT_AVAILABLE]
            );
        }
        $form->add(License::NOTE, TextType::class, ['label' => 'Poznámky', 'required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid->add(Attendance::ATTENDANCE, null, ['label' => 'Přítomen']);
        if ($this->isGranted('ALL')) {
            $datagrid->add(
                Attendance::PAID,
                'doctrine_orm_choice',
                [
                    'label' => 'Proplacena cesta',
                    'field_options' => ['choices' => PaidType::getChoices()],
                    'field_type' => ChoiceType::class,
                ]
            );
        }
        $datagrid->add(
            Attendance::USER,
            null,
            [
                'label' => 'Osoba',
                'admin_code' => 'admin.user',
            ],
            EntityType::class,
            [
                'class' => User::class,
                'choice_label' => 'getFullName',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.'.User::LASTNAME, 'ASC');
                },
            ]
        );
        $datagrid->add(
            Attendance::EVENT,
            null,
            ['label' => 'Akce'],
            EntityType::class,
            [
                'class' => Event::class,
                'query_builder' => function (EventRepository $er) {
                    $qb = $er->createQueryBuilder('e');

                    return $qb
                        ->orderBy('e.'.Event::DATE, 'ASC');
                },
            ]
        );
        $datagrid->add(
            Attendance::EVENT.'.'.Event::DATE,
            'doctrine_orm_date_range',
            ['label' => 'Datum'],
            DateRangePickerType::class,
            [
                'field_options_start' => [
                    'widget' => 'single_text',
                    'format' => "dd.MM.yyyy",
                    'attr' => ['class' => 'datepicker'],
                ],
                'field_options_end' => [
                    'widget' => 'single_text',
                    'format' => "dd.MM.yyyy",
                    'attr' => ['class' => 'datepicker'],
                ],
            ]
        );
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add(
            Attendance::EVENT,
            null,
            [
                'label' => 'Akce',
                'sort_field_mapping' => ['fieldName' => Event::DATE],
                'sort_parent_association_mappings' => [['fieldName' => Attendance::EVENT]],
                'sortable' => true,
            ]
        );
        $list->add(
            Attendance::USER,
            null,
            [
                'associated_property' => User::FULLNAME,
                'label' => 'Osoba',
                'sort_field_mapping' => ['fieldName' => User::LASTNAME],
                'sort_parent_association_mappings' => [['fieldName' => Attendance::USER]],
                'sortable' => true,
                'admin_code' => 'admin.user',
            ]
        );
        $list->add(
            Attendance::ATTENDANCE,
            'choice',
            [
                'choices' => AttendanceType::getReadableValues(),
                'label' => 'Přítomen',
                'header_class' => 'col-md-1',
            ]
        );
        $list->add(License::NOTE, null, ['label' => 'Poznámky']);
        if ($this->isGranted('ALL')) {
            $list->add(
                Attendance::PAID,
                'choice',
                [
                    'choices' => PaidType::getReadableValues(),
                    'editable' => true,
                    'header_class' => 'col-md-1',
                    'label' => 'Proplacena cesta',
                    'template' => 'CRUD/list_enum_with_custom_label.html.twig',
                ]
            );
        }
        $list->add(
            Attendance::EVENT_CASTS,
            null,
            [
                'label' => 'Funkce',
                'associated_property' => EventCast::LICENSE,
                'sort_field_mapping' => ['fieldName' => EventCast::ID],
                'sort_parent_association_mappings' => [['fieldName' => Attendance::EVENT_CASTS]],

            ]
        );
        $list->add(
            '_action',
            'actions',
            [
                'actions' => ['edit' => []],
                'label' => '',
            ]
        );
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->add('attendance');
    }

    protected function preSave(Attendance $object): void
    {
        if ($object->getAttendance() !== AttendanceType::YES) {
            $object->setPaid(PaidType::NOT_AVAILABLE);
            if ($object->getEventCasts()) {
                $object->getEventCasts()->clear();
            }
        } elseif ($object->getUser() && $object->getUser()->getStudent() && $object->getPaid() === PaidType::NOT_AVAILABLE) {
            $object->setPaid(PaidType::NOT_REFUND);
        }
    }
}
