<?php

namespace App\Admin;

use App\Entity\Tag;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class TagAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add(Tag::NAME);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add(Tag::NAME);
        foreach (Tag::$classes as $class => $value) {
            $listMapper->add($class, BooleanType::class, [
                'template' => 'CRUD/list_tag_available.html.twig',
                'tagClass' => $class,
            ]);
        }
        $listMapper->add(
            '_action',
            null,
            [
                'actions' => [ 'edit' => [], 'delete' => []],
                'label' => '',
            ]
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add(Tag::NAME);
        $formMapper->add(Tag::AVAILABLE, ChoiceType::class, ['multiple' => true, 'expanded' => true, 'choices' => Tag::$classes]);
        $formMapper->get(Tag::AVAILABLE)
            ->addModelTransformer(new CallbackTransformer(
                function ($classes) {
                    $array = [];
                    $actual = 0x01;
                    while ($classes >= $actual) {
                        if ($classes & $actual) {
                            $array[] = $actual;
                        }
                        $actual = $actual << 1;
                    }

                    return $array;
                },
                function ($classes) {
                    $num = 0;
                    foreach ($classes as $class) {
                        $num += $class;
                    }
                    return $num;
                }
            ));
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->remove('show');
    }
}
