<?php
/**
 * EventAdmin.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

namespace App\Admin;

use App\DBAL\Types\EventCaseType;
use App\Entity\Event;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateRangePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => Event::DATE,
    ];

    /**
     * @param $object Event
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        $object->setCast($object->getCast());
    }

    /**
     * @param $object Event
     * @throws \Sonata\AdminBundle\Exception\ModelManagerException
     */
    public function postPersist($object)
    {
        parent::postPersist($object);
        if ($object->getDateEnd() !== null) {
            $em = $this->getModelManager();
            $endDate = $object->getDateEnd()->modify('+1 day');
            /** @var \DateTime $datum */
            $datum = clone $object->getDate();
            $interval = $object->getDateInterval() ?: 1;
            $datum->modify("+$interval day");
            $i = 2;
            while ($datum <= $endDate) {
                $newObject = clone $object;
                $newObject->setName($newObject->getName().($interval === 1 ? " (den $i.)" : ""));
                $newObject->setDate($datum);
                $em->create($newObject);
                $datum->modify("+$interval day");
                $i++;
            }
            if ($interval === 1) {
                $object->setName($object->getName()." (den 1.)");
                $em->update($object);
            }
        }
    }

    /**
     * @param $object Event
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $object->setCast($object->getCast());
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        parent::validate($errorElement, $object);

        if ($object->getDateEnd() !== null && $object->getDate() > $object->getDateEnd()) {
            $errorElement->with('date_end')->addViolation('Konec musí být po začátku.')->end();
        }
        if ($object->getDateInterval() !== null && $object->getDateInterval() <= 0) {
            $errorElement->with('date_interval')->addViolation('Interval akcí musí být kladný..')->end();
        }
    }

    /**
     * @param $name
     * @param null|Event $object
     * @return bool|mixed
     */
    public function isGranted($name, $object = null)
    {
        $object = $object ? $object : $this->getSubject();
        $isGranted = parent::isGranted($name, $object);

        if ($isGranted && ('DELETE' === $name)) {
            return parent::isGranted('ALL') || ($object && (
                (($object->getCreatedByUser() && $object->getCreatedByUser()->getId() === $this->getUser()->getId()) ||
                !$object->getCreatedByUser())
            ));
        }

        return $isGranted;
    }

    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            [
                Event::DATE => [
                    'value' => [
                        'start' => date(self::DATE_FORMAT),
                    ],
                ],
            ],
            $this->datagridValues
        );

        return parent::getFilterParameters();
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Nazev', ['class' => 'col-xs-12 col-sm-7 col-lg-4']);
            $form->add(Event::NAME, TextType::class, ['label' => 'Název akce']);
            $form->add(
                Event::DATE,
                DateTimePickerType::class,
                [
                    'label' => 'Datum a čas',
                    'widget' => 'single_text',
                    'format' => self::DATETIME_FORMAT_JS,
                    'attr' => ['class' => 'datepicker'],
                    'dp_minute_stepping' => 15,
                    'dp_side_by_side' => true,
                ]
            );
        $form->end();
        $form->with('Typ', ['class' => 'pull-left col-xs-12 col-sm-5 col-lg-3 ']);
            $form->add(
                Event::TYPE,
                null,
                [
                    'label' => 'Typ akce',
                    'expanded' => true,
                ]
            );
        if ($this->isCurrentRoute('create')) {
            $form->add(
                'date_end',
                DatePickerType::class,
                [
                    'label' => 'Datum konce',
                    'widget' => 'single_text',
                    'format' => self::DATE_FORMAT_JS,
                    'attr' => ['class' => 'datepicker'],
                    'required' => false,
                ]
            );
            $form->add(
                'date_interval',
                IntegerType::class,
                [
                    'label' => 'Interval akcí ve dnech',
                    'required' => false,
                ]
            );
        }
        $form->end();
        $form->with('Obsazení', ['class' => 'pull-right col-xs-12 col-lg-5']);
            $form->add(
                Event::CAST,
                CollectionType::class,
                [
                    'label' => 'Personální obsazení',
                    'type_options' => [
                        'by_reference' => true,
                    ],
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable'  => 'position',
                ]
            );
        $form->end();
        $form->with('Poznámky', ['class' => 'col-xs-12 col-lg-7']);
            $form->add(
                Event::NOTE,
                CKEditorType::class,
                [
                    'label' => false,
                    'required' => false,
                ]
            );
        $form->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid->add(Event::NAME, null, ['label' => 'Název']);
        $datagrid->add(
            Event::TYPE,
            'doctrine_orm_choice',
            [
                'label' => 'Typ',
                'field_options' => [
                    'choices' => EventCaseType::getChoices(),
                    'multiple' => true,
                ],
                'field_type' => ChoiceType::class,
            ]
        );
        $datagrid->add(
            Event::DATE,
            'doctrine_orm_date_range',
            ['label' => 'Datum'],
            DateRangePickerType::class,
            [
                'field_options_start' => [
                    'widget' => 'single_text',
                    'format' => self::DATE_FORMAT_JS,
                    'attr' => ['class' => 'datepicker'],
                ],
                'field_options_end' => [
                    'widget' => 'single_text',
                    'format' => self::DATE_FORMAT_JS,
                    'attr' => ['class' => 'datepicker'],
                ],
            ]
        );
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add(Event::NAME, null, ['label' => 'Název', 'header_style' => 'width: 15%;']);
        $list->add(Event::DATE, 'datetime', ['label' => 'Datum', 'format' => self::DATETIME_FORMAT]);
        $list->add(Event::TYPE, 'choice', ['label' => 'Typ', 'choices' => EventCaseType::getReadableValues()]);
        $list->add(
            '_action',
            'actions',
            [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'clone' => [
                        'template' => 'CRUD/list__action_clone.html.twig',
                    ],
                ],
                'label' => '',
            ]
        );
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->with('Detaily', ['class' => 'col-xs-12 col-sm-6']);
            $show->add(Event::NAME, null, ['label' => 'Název']);
            $show->add(Event::DATE, null, ['label' => 'Datum', 'format' => self::DATETIME_FORMAT]);
            $show->add(Event::TYPE, 'choice', ['label' => 'Typ', 'choices' => EventCaseType::getReadableValues()]);
        $show->end();
        $show->with('Obsazení', ['class' => 'pull-right col-xs-12 col-sm-6']);
            $show->add(Event::CAST, null, ['label' => 'Personální obsazení']);
        $show->end();
        $show->with('Poznámky', ['class' => 'col-xs-12']);
            $show->add(Event::NOTE, 'html', ['label' => 'Poznámky']);
        $show->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter().'/clone');
    }
}
