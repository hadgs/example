<?php
/*
 * LicenseAdmin.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\Admin;

use App\Entity\License;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LicenseAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => License::NAME,
    ];

    protected function configureFormFields(FormMapper $form)
    {
        $form->add(License::NAME, TextType::class, ['label' => 'Název']);
        $form->add(License::ABBREVIATION, TextType::class, ['label' => 'Zkratka']);
        $form->add(License::NOTE, TextType::class, ['label' => 'Poznámky', 'required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid->add(License::NAME, null, ['label' => 'Název']);
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add(License::NAME, null, ['label' => 'Název']);
        $list->add(License::ABBREVIATION, null, ['label' => 'Zkratka']);
        $list->add(License::NOTE, null, ['label' => 'Poznámky']);
        $list->add(
            '_action',
            'actions',
            [
                'actions' => ['show' => [], 'edit' => []],
                'label' => '',
            ]
        );
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add(License::NAME, null, ['label' => 'Název']);
        $show->add(License::ABBREVIATION, null, ['label' => 'Zkratka']);
        $show->add(License::NOTE, 'html', ['label' => 'Poznámky']);
    }
}
