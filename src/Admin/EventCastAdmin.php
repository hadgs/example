<?php

namespace App\Admin;

use App\Entity\EventCast;
use App\Entity\License;
use App\Repository\LicenseRepository;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventCastAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add(
            EventCast::LICENSE,
            EntityType::class,
            [
                'label' => 'Funkce',
                'class' => License::class,
                'query_builder' => function (LicenseRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.'.License::NAME, 'ASC');
                },
            ]
        );
        $form->add(EventCast::EVENT, ModelHiddenType::class);
        $form->add(EventCast::NOTE, TextType::class, ['label' => 'Poznámka', 'required' => false]);
        $form->add(EventCast::POSITION);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['create']);
    }
}
