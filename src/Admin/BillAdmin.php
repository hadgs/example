<?php
/*
 * BillAdmin.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\Admin;

use App\DBAL\Types\AccountType;
use App\DBAL\Types\BillDirectionType;
use App\DBAL\Types\BillStateType;
use App\Entity\Bill;
use App\Entity\BillCategory;
use App\Entity\Tag;
use App\Entity\User;
use App\Repository\BillCategoryRepository;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class BillAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => Bill::DATE_PAID,
    ];

    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            [
                'datePaid' => [
                    'value' => [
                        'start' => date('Y', strtotime('first day of january this year')),
                    ],
                ],
            ],
            $this->datagridValues
        );

        return parent::getFilterParameters();
    }

    /**
     * @param $object Bill
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        if (!$object->getDatePaid() && (BillStateType::PAID === $object->getState() || BillStateType::PAID_PENDING_BILL === $object->getState())) {
            $object->setDatePaid(new \DateTime());
        }
        if ($object->getCategory() && $object->getCategory()->getDirection() === BillDirectionType::EXPENSE && $object->getAmount() > 0) {
            $object->setAmount($object->getAmount() * -1);
        }
        if ($object->getCategory()->getDirection() !== BillDirectionType::TRANSFER) {
            $object->setAccountTo(null);
        } elseif ($object->getCategory()->getDirection() === BillDirectionType::TRANSFER && $object->getAmount() < 0) {
            $object->setAmount($object->getAmount() * -1);
        }
    }

    /**
     * @param $object Bill
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        if (!$object->getDatePaid() && (BillStateType::PAID === $object->getState() || BillStateType::PAID_PENDING_BILL === $object->getState())) {
            $object->setDatePaid(new \DateTime());
        }
        if ($object->getCategory() && $object->getCategory()->getDirection() === BillDirectionType::EXPENSE && $object->getAmount() > 0) {
            $object->setAmount($object->getAmount() * -1);
        }
        if ($object->getCategory()->getDirection() !== BillDirectionType::TRANSFER) {
            $object->setAccountTo(null);
        } elseif ($object->getCategory()->getDirection() === BillDirectionType::TRANSFER && $object->getAmount() < 0) {
            $object->setAmount($object->getAmount() * -1);
        }
    }

    public function getExportFields()
    {
        return [
            Bill::NAME, Bill::AMOUNT.'AsString', Bill::STATE.'AsString', Bill::ACCOUNT.'AsString', Bill::CATEGORY, Bill::DATE_PAID
        ];
    }

    public function getDataSourceIterator()
    {
        $iterator = parent::getDataSourceIterator();
        $iterator->setDateTimeFormat('d.m.Y');
        return $iterator;
    }

    public function getTemplate($name)
    {
        if ($name == 'list') {
            return 'list.html.twig';
        }

        return parent::getTemplate($name);
    }

    protected function configureFormFields(FormMapper $form)
    {
        $closure = $this->getRepository(BillCategory::class)->findBy([], [BillCategory::NAME => 'ASC']);
        $form->with('Main', ['class' => 'col-xs-12 col-md-6 col-lg-4']);
            $form->add(Bill::NAME, null, ['label' => 'Účel']);
            $form->add(Bill::NUMBER, null, ['label' => 'Číslo', 'required' => false]);
            $form->add(
                Bill::DOCUMENT_FILE,
                VichFileType::class,
                [
                    'label' => 'Doklad',
                    'required' => false,
                    'download_label' => 'download',
                ]
            );
        $form->end();
        $form->with('Finance', ['class' => 'col-xs-12 col-md-6 col-lg-4']);
            $form->add(Bill::AMOUNT, null, ['label' => 'Částka']);
            $form->add(
                Bill::CATEGORY,
                ChoiceFieldMaskType::class,
                [
                    'required' => true,
                    'label' => 'Kategorie',
                    'group_by' => function ($choiceValue, $key, $value) {
                        /** @var $choiceValue \App\Entity\BillCategory */
                        if (BillDirectionType::getReadableValue($choiceValue->getDirection()) === $choiceValue->getName()) {
                            return null;
                        }
                        return BillDirectionType::getReadableValue($choiceValue->getDirection());
                    },
                    'preferred_choices'=> function ($choiceValue, $key, $value) {
                        /** @var $choiceValue \App\Entity\BillCategory */
                        if (BillDirectionType::getReadableValue($choiceValue->getDirection()) === $choiceValue->getName()) {
                            return true;
                        }
                        return false;
                    },
                    'choices' => $closure,
                    'choice_label' => function(BillCategory $category, $key, $value) {
                        return $category->getName();
                    },
                    'choice_value' => function (BillCategory $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'map' => [
                        '21' => [Bill::ACCOUNT_TO],
                    ],
                ]
            );
            $form->add(Bill::ACCOUNT, null, ['label' => 'Účet']);
            $form->add(Bill::ACCOUNT_TO, null, ['label' => 'Cílový účet']);
        $form->end();
        $form->with('Basic', ['class' => 'col-xs-12 col-md-6 col-lg-4']);
            $form->add(Bill::STATE, null, ['label' => 'Stav']);
            $form->add(Bill::DATE_PAID, DatePickerType::class, [
                'label' => 'Zaplaceno',
                'widget' => 'single_text',
                'required' => true,
                'format' => AbstractAdmin::DATE_FORMAT_JS,
            ]);
            $form->add(
                Bill::TAGS,
                ModelType::class,
                [
                    'label' => 'Štítky',
                    'multiple' => true,
                    'property' => Tag::NAME,
                    'required' => false,
                    'query' => $this->getRepository(Tag::class)->findForClass(Bill::class),
                ]
            );
            $form->add(
                Bill::REQUESTING_USER,
                null,
                [
                    'label' => 'Uživatel',
                    'choice_label' => 'getFullName',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                            ->orderBy('e.'.User::LASTNAME, 'ASC');
                    },
                ],
                [
                    'admin_code' => 'admin.user',
                ]
            );
        $form->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid->add(Bill::NAME, null, ['label' => 'Účel']);
        $datagrid->add(
            Bill::CATEGORY,
            null,
            ['label' => 'Kategorie'],
            null,
            [
                'class' => BillCategory::class,
                'choice_label' => 'getName',
                'query_builder' => function (BillCategoryRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.'.BillCategory::NAME, 'ASC');
                },
                'group_by' => function ($choiceValue, $key, $value) {
                    /** @var $choiceValue \App\Entity\BillCategory */
                    if (BillDirectionType::getReadableValue($choiceValue->getDirection()) === $choiceValue->getName()) {
                        return null;
                    }
                    return BillDirectionType::getReadableValue($choiceValue->getDirection());
                },
                'preferred_choices'=> function ($choiceValue, $key, $value) {
                    /** @var $choiceValue \App\Entity\BillCategory */
                    if (BillDirectionType::getReadableValue($choiceValue->getDirection()) === $choiceValue->getName()) {
                        return true;
                    }
                    return false;
                },
                'multiple' => true,
            ]
        );
        $datagrid->add(
            Bill::STATE,
            'doctrine_orm_choice',
            [
                'label' => 'Stav',
                'field_options' => [
                    'choices' => BillStateType::getChoices(),
                    'multiple' => true,
                ],
                'field_type' => ChoiceType::class,
            ]
        );
        $datagrid->add(
            Bill::ACCOUNT,
            'doctrine_orm_choice',
            [
                'label' => 'Účet',
                'field_options' => [
                    'choices' => AccountType::getChoices(),
                    'multiple' => true,
                ],
                'field_type' => ChoiceType::class,
            ]
        );
        $datagrid->add(
            Bill::TAGS,
            null,
            ['label' => 'Štítky'],
            null,
            [
                'choice_label' => Tag::NAME,
                'query_builder' => $this->getRepository(Tag::class)->findForClass(Bill::class),
                'multiple' => true,
            ]
        );
        $datagrid->add(
            Bill::DATE_PAID,
            'doctrine_orm_date_range',
            ['label' => 'Datum zaplacení'],
            DateRangePickerType::class,
            [
                'field_options_start' => [
                    'widget' => 'single_text',
                    'format' => 'yyyy',
                    'attr' => ['class' => 'datepicker'],
                    'dp_view_mode'          => 'years',
                    'dp_min_view_mode'      => 'years',
                    'dp_use_current'        => false,
                ],
                'field_options_end' => [
                    'widget' => 'single_text',
                    'format' => 'yyyy',
                    'attr' => ['class' => 'datepicker'],
                    'dp_view_mode'          => 'years',
                    'dp_min_view_mode'      => 'years',
                    'dp_use_current'        => false,
                ],
            ]
        );
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add(Bill::NAME, null, ['label' => 'Účel']);
        $list->add(Bill::NUMBER, null, ['label' => 'Číslo']);
        $list->add(
            Bill::CATEGORY,
            null,
            [
                'label' => 'Kategorie',
                'sort_field_mapping' => ['fieldName' => BillCategory::NAME],
                'sort_parent_association_mappings' => [['fieldName' => Bill::CATEGORY]],
                'sortable' => true,
            ]
        );
        $list->add(
            Bill::AMOUNT,
            null,
            [
                'label' => 'Částka',
                'template' => 'CRUD/list_currency.html.twig',
                'currency' => 'Kč',
            ]
        );
        $list->add(
            Bill::STATE,
            'choice',
            [
                'choices' => BillStateType::getReadableValues(),
                'editable' => true,
                'header_class' => 'col-md-1',
                'label' => 'Stav',
                'template' => 'CRUD/list_enum_with_custom_label.html.twig',
            ]
        );
        $list->add(
            Bill::ACCOUNT,
            'choice',
            [
                'choices' => AccountType::getReadableValues(),
                'label' => 'Účet',
                'template' => 'CRUD/list_account.html.twig',
            ]
        );
        $list->add(
            Bill::TAGS,
            null,
            [
                'label' => 'Štítky',
                'associated_property' => Tag::NAME,
                'sort_field_mapping' => ['fieldName' => Tag::NAME],
                'sort_parent_association_mappings' => [['fieldName' => Bill::TAGS]],
                'sortable' => true,
            ]
        );
        $list->add(
            Bill::REQUESTING_USER,
            null,
            [
                'label' => 'Uživatel',
                'associated_property' => User::FULLNAME,
                'sort_field_mapping' => ['fieldName' => User::LASTNAME],
                'sort_parent_association_mappings' => [['fieldName' => Bill::REQUESTING_USER]],
                'sortable' => true,
                'admin_code' => 'admin.user',
            ]
        );
        $list->add(Bill::DOCUMENT_NAME, null, ['label' => 'Doklad', 'template' => 'CRUD/list_file.html.twig']);
        $list->add(Bill::DATE_PAID, null, ['label' => 'Datum zaplacení', 'format' => self::DATE_FORMAT]);
        $list->add(
            '_action',
            'actions',
            [
                'actions' => ['show' => [], 'edit' => []],
                'label' => '',
            ]
        );
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }
}
