<?php

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\Form\Type\DatePickerType;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UserAdmin extends BaseUserAdmin
{
    protected $maxPerPage = 256;
    
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => User::LASTNAME,
    ];

    public function isGranted($name, $object = null)
    {
        $object = $object ? $object : $this->getSubject();
        $isGranted = parent::isGranted($name, $object);

        if ($isGranted && ('EDIT' === $name)) {
            return $object && (
                $object->getId() === $this->getUser()->getId() ||
                parent::isGranted('ALL')
            );
        }
        if ($isGranted && ('VIEW' === $name)) {
            return $object && (
                $object->getId() === $this->getUser()->getId() ||
                parent::isGranted('ALL')
            );
        }

        return $isGranted;
    }

    public function configureBatchActions($actions)
    {
        $actions['mail'] = [
            'ask_confirmation' => false,
            'label' => 'Odeslat e-mail',
        ];
        $actions['sms'] = [
            'ask_confirmation' => false,
            'label' => 'Odeslat SMS',
        ];

        return $actions;
    }

    protected function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper): void
    {
        parent::configureDatagridFilters($filterMapper);
        $filterMapper->remove('groups');
        $filterMapper->add('groups', null, [], EntityType::class, ['multiple' => true]);
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->add(User::PROFILE_IMAGE, null, ['template' => 'CRUD/list_image.html.twig', 'width' => 50, 'height' => 50]);
        $list->add('lastname');
        $list->add('firstname');
        $list->add('email', 'email');
        $list->add('phone', null, ['template' => 'CRUD/list_phone.html.twig']);
        if ($this->isGranted('ALL')) {
            $list->add('groups');
            $list->add('student');
            $list->add('enabled', null, ['editable' => $this->isGranted('ALL')]);
        }

        $list->add(
            '_action',
            'actions',
            [
                'actions' => ['show' => [], 'edit' => []],
                'label' => '',
            ]
        );

        if ($this->getUser()->hasRole('ROLE_ALLOWED_TO_SWITCH')) {
            $list
                ->add('impersonating', 'string', ['template' => '@SonataUser/Admin/Field/impersonating.html.twig'])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        parent::configureShowFields($showMapper);

        $showMapper
            ->with('Provozní')
            ->add(User::STUDENT)
            ->add(User::LICENSE)
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        parent::configureFormFields($formMapper);

        $formMapper->removeGroup('Social', 'User', true);
        $formMapper->remove('plainPassword');
        $formMapper->remove('website');
        $formMapper->remove('biography');
        $formMapper->remove('locale');
        $formMapper->remove('timezone');
        $formMapper->remove('dateOfBirth');
        $formMapper
            ->tab('User')
                ->with('Profile')
                    ->add('dateOfBirth', DatePickerType::class, [
                        'years' => range(1900, (new \DateTime())->format('Y')),
                        'dp_min_date' => '1-1-1900',
                        'dp_max_date' => (new \DateTime())->format('c'),
                        'required' => false,
                        'format' => 'd.M.y',
                        'widget' => 'single_text',
                        'attr' => ['class' => 'datepicker'],
                    ])
                ->end()
                ->with('General')
                    ->add('plainPassword', RepeatedType::class, [
                        'type' => PasswordType::class,
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                        'first_options' => ['label' => 'form.label_password'],
                        'second_options' => ['label' => 'form.label_repeated_password'],
                    ])
                ->end()
            ->end();

        if (!$this->isGranted('ALL')) {
            // Only ROLE_SUPER_ADMIN can edit the fields in Status, Groups and Roles groups
            $formMapper->removeGroup('Status', 'Security', true);
            $formMapper->removeGroup('Groups', 'Security', true);
            $formMapper->removeGroup('Roles', 'Security', true);
        } else {
            $formMapper
                ->tab('Provozní')
                ->with('Oprávnění')
                ->add('imageFile', VichFileType::class, ['required' => false])
                ->add(
                    User::STUDENT,
                    BooleanType::class,
                    [
                        'label' => 'Student',
                        'transform' => true,
                        'expanded' => true,
                    ]
                )
                ->add(
                    User::LICENSE,
                    ModelType::class,
                    [
                        'label' => 'Oprávnění',
                        'multiple' => true,
                        'btn_add' => false,
                    ]
                )
                ->end()
            ->end();
        }
    }
}
