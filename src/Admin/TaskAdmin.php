<?php

namespace App\Admin;

use App\DBAL\Types\PriorityType;
use App\Entity\Task;
use App\Entity\Tag;
use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TaskAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => Task::DATE_DUE,
    ];

    public static function filterByDoneCallback(ProxyQuery $proxyQuery, $alias, $field, $data)
    {
        $data = $data['value'];
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $proxyQuery->getQueryBuilder();
        $alias = $queryBuilder->getRootAliases()[0];

        if ($data == 1) {
            $queryBuilder->andWhere($alias.'.'.Task::DATE_DONE.' is not null');

            return true;
        } elseif ($data == 2) {
            $queryBuilder->andWhere($alias.'.'.Task::DATE_DONE.' is null');

            return true;
        }

        return false;
    }

    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            [
                Task::DATE_DONE => [
                    'value' => 2
                ],
            ],
            $this->datagridValues
        );

        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $queryBuilder = $this->getModelManager()->getEntityManager($this->getClass())->createQueryBuilder();
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder->select('p')
            ->from($this->getClass(), 'p')
            ->leftJoin('p.'.Task::RESPONSIBLE_USERS, 'pu', Join::WITH)
            ->where('p.'.Task::CREATED_BY_USER.' = '.$this->getUser()->getId())
            ->orWhere('pu.'.User::ID.' = '.$this->getUser()->getId());

        $proxyQuery = new ProxyQuery($queryBuilder);

        return $proxyQuery;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Basic', ['class' => 'col-xs-12 col-md-6']);
            $form->add(Task::NAME, TextType::class, ['label' => 'Název', 'required' => true]);
            $form->add(Task::DATE_DUE, DatePickerType::class, [
                'label' => 'Do kdy',
                'widget' => 'single_text',
                'format' => AbstractAdmin::DATE_FORMAT_JS,
            ]);
            $form->add(Task::TIME_ESTIMATE, NumberType::class, ['label' => 'Časový odhad', 'required' => true, 'sonata_help' => 'Odhad v hodinách']);
            $form->add(Task::DATE_DONE, DatePickerType::class, [
                'label' => 'Hotovo',
                'widget' => 'single_text',
                'required' => false,
                'format' => AbstractAdmin::DATE_FORMAT_JS,
            ]);
        $form->end();
        $form->with('Advance', ['class' => 'col-xs-12 col-md-6']);
            $form->add(Task::PRIORITY, ChoiceType::class, ['label' => 'Priorita', 'required' => true, 'choices' => PriorityType::getChoices()]);
            $form->add(
                Task::RESPONSIBLE_USERS,
                ModelType::class,
                [
                    'label' => 'Zodpovědní',
                    'required' => false,
                    'multiple' => true,
                    'property' => 'fullName',
                    'query' => $this->modelManager->getEntityManager(User::class)->createQueryBuilder('e')->select('e')->from(User::class, 'e')->orderBy('e.'.User::LASTNAME, 'ASC'),
                    'preferred_choices' => function ($value, $key) {
                        return $value == $this->getUser()->getId();
                    },
                    'btn_add' => false,
                ],
                [
                    'admin_code' => 'admin.user',
                ]
            );
            $form->add(
                Task::TAGS,
                ModelType::class,
                [
                    'label' => 'Štítky',
                    'multiple' => true,
                    'property' => Tag::NAME,
                    'required' => true,
                    'query' => $this->getRepository(Tag::class)->findForClass(Task::class),
                ]
            );
            $form->add(
                Task::PARENT,
                ModelListType::class,
                [
                    'label' => 'Nadřazený úkol',
                    'required' => false,
                    //~ 'property' => Task::NAME,
                    //~ 'query' => $this->modelManager->getEntityManager(Task::class)->createQueryBuilder('e')->select('e')->from(Task::class, 'e')->orderBy('e.'.Task::NAME, 'ASC'),
                ]
            );
        $form->end();
        $form->with('Note', ['class' => 'col-xs-12']);
            $form->add(Task::NOTE, CKEditorType::class, ['label' => 'Popis', 'required' => false]);
            $form->add(Task::POSITION_CORRECTION, HiddenType::class, ['data' => '0']);
        $form->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid->add(Task::NAME, null, ['label' => 'Název']);
        $datagrid->add(
            Task::DATE_DONE,
            'doctrine_orm_callback',
            [
                'label' => 'Zobrazit úkoly',
                'callback' => [$this, 'filterByDoneCallback'],
            ],
            ChoiceType::class,
            [
                'choices' => ['Pouze hotové' => 1, 'Pouze nehotové' => 2],
                'placeholder' => 'Všechny',
            ]
        );
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add(Task::NAME, null, ['label' => 'Název']);
        $list->add(Task::TAGS, null, [
            'associated_property' => Tag::NAME,
            'label' => 'Štítky',
        ]);
        $list->add(Task::DATE_DUE, null, ['label' => 'Do kdy']);
        $list->add(Task::DATE_DONE, null, ['label' => 'Hotovo']);
        $list->add(Task::PRIORITY, 'choice', ['label' => 'Priorita', 'choices' => PriorityType::getReadableValues()]);
        $list->add(Task::PARENT, null, ['label' => 'Nadřazený', 'associated_property' => Task::NAME]);
        $list->add('_action', 'actions', ['actions' => [
            'edit' => [],
            'show' => [],
            // 'move' => ['template' => 'CRUD/_sort.html.twig'],
        ],]);
    }

//    protected function configureRoutes(RouteCollection $collection)
//    {
//        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
//    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $menu->addChild('Přihlásit k Google', array('uri' => $this->getRouteGenerator()->generate('google_controlller')));
    }

    public function getTemplate($name)
    {
        if ($name == 'list') {
            return 'CRUD/list_tasks.html.twig';
        }

        return parent::getTemplate($name);
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
        $service = $this->getConfigurationPool()->getContainer()->get('service.google');
        $service->createTask($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $service = $this->getConfigurationPool()->getContainer()->get('service.google');
        $service->updateTask($object);
    }

    public function preRemove($object)
    {
        parent::preRemove($object);
        if ($object->getGoogleId()) {
            $service = $this->getConfigurationPool()->getContainer()->get('service.google');
            $service->deleteTask($object);
        }
    }
}
