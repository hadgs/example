<?php
/*
 * AbstractAdmin.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\Admin;

use App\Entity\Column\CreatedAtInterface;
use App\Entity\Column\CreatedByUserInterface;
use App\Entity\Column\UpdatedAtInterface;
use App\Entity\Column\UpdatedByUserInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin as BaseAdmin;

class AbstractAdmin extends BaseAdmin
{
    protected const DATE_FORMAT = 'd.m.Y';
    protected const DATETIME_FORMAT = 'd.m.Y H:i';
    protected const DATE_FORMAT_JS = "dd.MM.yyyy";
    protected const DATETIME_FORMAT_JS = 'dd.MM.yyyy HH:mm';

    public function prePersist($object)
    {
        parent::prePersist($object);
        if ($object instanceof CreatedAtInterface) {
            $object->setCreatedAt(new \DateTime());
        }
        if ($object instanceof UpdatedAtInterface) {
            $object->setUpdatedAt(new \DateTime());
        }
        if ($object instanceof CreatedByUserInterface) {
            $object->setCreatedByUser($this->getUser());
        }
        if ($object instanceof UpdatedByUserInterface) {
            $object->setUpdatedByUser($this->getUser());
        }
    }

    public function postPersist($object)
    {
        parent::postPersist($object);
        $this->getLogger()->info('Uživatel '.$this->getUser()->getFullname().' vytvořil záznam '.$this->getClassnameLabel().' s ID '.$object->getId());
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        if ($object instanceof UpdatedAtInterface) {
            $object->setUpdatedAt(new \DateTime());
        }
        if ($object instanceof UpdatedByUserInterface) {
            $object->setUpdatedByUser($this->getUser());
        }
    }

    public function postUpdate($object)
    {
        parent::postUpdate($object);
        $this->getLogger()->info('Uživatel '.$this->getUser()->getFullname().' editoval záznam '.$this->getClassnameLabel().' s ID '.$object->getId());
    }

    public function postRemove($object)
    {
        parent::postRemove($object);
        $this->getLogger()->info('Uživatel '.$this->getUser()->getFullname().' odstranil záznam '.$this->getClassnameLabel().' s ID '.$object->getId());
    }

    public function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    public function getRepository(string $repositoryClass): ObjectRepository
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($repositoryClass);
    }

    public function getLogger()
    {
        return $this->getConfigurationPool()->getContainer()->get('service.logger')->getLogger();
    }

    public function getMailer()
    {
        return $this->getConfigurationPool()->getContainer()->get('service.mailer');
    }
}
