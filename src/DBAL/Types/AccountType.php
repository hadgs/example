<?php
/*
 * AccountType.php
 *
 * Copyright 2018 digital <digital@digital-ThinkPad-T420>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class AccountType
 */
final class AccountType extends AbstractEnumType
{
    public const BANK = 'bank';
    public const CASH_BIG = 'cashBig';
    public const CASH_SMALL = 'cashSmall';
    public const BLACK_BAG = 'blackBag';
    public const BROWN_BAG = 'brownBag';

    protected static $choices = [
        self::BANK => 'Banka',
        self::CASH_BIG => 'Pokladna velká',
        self::CASH_SMALL => 'Pokladna malá',
        self::BLACK_BAG => 'Černá brašna',
        self::BROWN_BAG => 'Hnědá brašna',
    ];
}
