<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AttendanceType extends AbstractEnumType
{
    public const YES = 'yes';
    public const NO = 'no';
    public const MAYBE = 'maybe';

    protected static $choices = [
        self::YES => 'Ano',
        self::NO => 'Ne',
        self::MAYBE => 'Možná',
    ];
}
