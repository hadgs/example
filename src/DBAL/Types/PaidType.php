<?php
/*
 * PaidType.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class PaidType
 */
final class PaidType extends AbstractEnumType
{
    public const NOT_AVAILABLE = 'canNotClaim';
    public const NOT_REFUND = 'notRefund';
    public const REFUND = 'refund';

    protected static $choices = [
        self::NOT_AVAILABLE => 'Nemá nárok',
        self::NOT_REFUND => 'Není proplaceno',
        self::REFUND => 'Proplaceno',
    ];

    /**
     * @return array
     */
    public static function getChoicesWithoutCanNotClaim()
    {
        return \array_flip(array_diff(self::$choices, [self::NOT_AVAILABLE => 'Nemá nárok']));
    }
}
