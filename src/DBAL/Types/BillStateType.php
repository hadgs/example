<?php
/*
 * BillStateType.php
 *
 * Copyright 2018 digital <digital@digital-ThinkPad-T420>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class BillStateType
 */
final class BillStateType extends AbstractEnumType
{
    public const FRESH = 'new';
    public const RECEIVED = 'received';
    public const REJECTED = 'rejected';
    public const PENDING_BILL = 'pendingBill';
    public const PENDING_PAYMENT = 'pendingPayment';
    public const PAID_PENDING_BILL = 'paidPendingBill';
    public const PAID = 'paid';
    public const EXPECTED = 'expected';

    public const MAPPING_ACTUAL = 'actual';
    public const MAPPING_DISPONIBLE = 'disponible';
    public const MAPPING_REJECTED = 'rejected';

    protected static $choices = [
        self::FRESH => 'Nový',
        self::EXPECTED => 'Předpokládaný',
        self::RECEIVED => 'Přijato',
        self::REJECTED => 'Odmítnuto',
        self::PENDING_BILL => 'Přijato, čeká na doklad',
        self::PENDING_PAYMENT => 'Přijato, čeká na platbu',
        self::PAID_PENDING_BILL => 'Zaplaceno, čeká na doklad',
        self::PAID => 'Zaplaceno',
    ];

    public static function getMapping(string $billState) {
        return [
            self::FRESH => self::MAPPING_DISPONIBLE,
            self::EXPECTED => self::MAPPING_DISPONIBLE,
            self::RECEIVED => self::MAPPING_DISPONIBLE,
            self::REJECTED => self::MAPPING_REJECTED,
            self::PENDING_BILL => self::MAPPING_DISPONIBLE,
            self::PENDING_PAYMENT => self::MAPPING_DISPONIBLE,
            self::PAID_PENDING_BILL => self::MAPPING_ACTUAL,
            self::PAID => self::MAPPING_ACTUAL,
        ][$billState];
    }
}
