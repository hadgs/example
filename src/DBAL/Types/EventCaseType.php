<?php
/*
 * EventCaseType.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class EventCaseType extends AbstractEnumType
{
    public const PUBLIC_RIDES = 'publicRides';
    public const SPECIAL_RIDES = 'specialRides';
    public const BRIGADE_DAYS = 'brigadeDays';
    public const OTHER = 'other';

    protected static $choices = [
        self::PUBLIC_RIDES => 'Veřejné jízdy',
        self::SPECIAL_RIDES => 'Objednané jízdy',
        self::BRIGADE_DAYS => 'Brigády',
        self::OTHER => 'Ostatní',
    ];
}
