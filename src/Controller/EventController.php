<?php

namespace App\Controller;

use App\Entity\Event;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventController extends CRUDController
{
    public function cloneAction($id)
    {
        if (!$this->admin->hasAccess('create')) {
            throw new NotFoundHttpException('Not found');
        }

        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        // Be careful, you may need to overload the __clone method of your object
        // to set its id to null !
        /** @var Event $object */
        $clonedObject = clone $object;

        $clonedObject->setName($object->getName().' (Clone)');
        $clonedObject->setCreatedByUser($this->admin->getUser());

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

        return new RedirectResponse($this->admin->generateUrl('edit', ['id' => $clonedObject->getId()]));
    }
}
