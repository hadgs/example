<?php

namespace App\Controller;

use App\DBAL\Types\AttendanceType;
use App\DBAL\Types\PaidType;
use App\Entity\Attendance;
use App\Entity\Event;
use App\Entity\User;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AttendanceController extends CRUDController
{
    public function attendanceAction(Request $request)
    {
        $userId = $request->get('userId');
        $eventId = $request->get('eventId');
        $attendance = $request->get('attendance');
        $event = $this->getDoctrine()->getRepository(Event::class)->find($eventId);
        $params = ['type' => $event->getType(), 'events' => 15];
        if (!in_array($attendance, AttendanceType::getValues())) {
            $this->addFlash(
                'sonata_flash_error',
                $this->trans(
                    'flash_create_error',
                    ['%name%' => $this->escapeHtml('Účast')],
                    'SonataAdminBundle'
                )
            );

            return $this->redirectToRojList($params);
        }

        /** @var Attendance $object */
        $object = $this->admin->getNewInstance();
        $object->setUser($this->getDoctrine()->getRepository(User::class)->find($userId));
        $object->setEvent($event);
        $object->setAttendance($attendance);
        $object->setPaid(PaidType::NOT_AVAILABLE);

        /** @var Attendance $newObject */
        $newObject = $this->admin->create($object);

        $this->addFlash(
            'sonata_flash_success',
            $this->trans(
                'flash_create_success',
                ['%name%' => $this->escapeHtml($this->admin->toString($newObject))],
                'SonataAdminBundle'
            )
        );

        return $this->redirectToRojList($params);
    }

    /**
     * Redirect the user depend on this choice.
     *
     * @param Attendance $object
     *
     * @return RedirectResponse
     */
    protected function redirectTo($object)
    {
        $request = $this->getRequest();
        $params = ['type' => $object->getEvent()->getType()];

        if (null !== $request->get('btn_update_and_list')) {
            return $this->redirectToRojList($params);
        }
        if (null !== $request->get('btn_create_and_list')) {
            return $this->redirectToRojList($params);
        }

        return parent::redirectTo($object);
    }

    private function redirectToRojList(array $parameters)
    {
        $rojAdmin = $this->get('admin.roj');

        if ($filter = $rojAdmin->getFilterParameters()) {
            $parameters['filter'] = $filter;
        }

        return $this->redirect($rojAdmin->generateUrl('list', $parameters));
    }
}
