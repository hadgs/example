<?php
/*
 * ApiController.php
 *
 * Copyright 2018 Martin <martin@martin-MS-7A37>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

namespace App\Controller;

use App\Entity\Event;
use App\Entity\EventCast;
use App\Entity\License;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{

    /**
     * Constructor of class ApiController.
     *
     * @return void
     */
    public function __construct()
    {
        // ...
    }

    /**
     * @Route("/api/get-event-cast-on-attendance")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function eventCastOnAttendanceAction(Request $request)
    {
        $qb = $this->getDoctrine()->getRepository(EventCast::class)->createQueryBuilder('e');
        $expr = $qb->expr();
        $qb->join('e.'.EventCast::EVENT, 'ee');
        $qb->join('e.'.EventCast::LICENSE, 'el');
        $qb->join('el.'.License::USERS, 'elu');
        $qb->andWhere($expr->eq('e.'.EventCast::EVENT, $request->query->get("eventId")));
        $qb->andWhere($expr->eq('elu.id', $request->query->get("userId")));
        $qb->orderBy('e.'.EventCast::POSITION, 'ASC');

        $responseArray = [];
        foreach ($qb->getQuery()->getResult() as $row) {
            $responseArray[] = array(
                "id" => $row->getId(),
                "label" => $row->__toString(),
            );
        }
        $val['status'] = 'OK';
        $val['more'] = false;
        $val['items'] = $responseArray;

        return new JsonResponse($val);
    }
}
