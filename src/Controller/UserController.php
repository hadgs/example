<?php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends CRUDController
{
    public function batchActionMail(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $emails = [];
        foreach ($selectedModelQuery->execute() as $selectedModel) {
            if ($selectedModel->getEmail()) {
                $emails[] = $selectedModel->getEmail();
            }
        }

        return new RedirectResponse('mailto:'.implode(",", $emails));
    }

    public function batchActionSms(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $phones = [];
        foreach ($selectedModelQuery->execute() as $selectedModel) {
            if ($selectedModel->getPhone()) {
                $phones[] = $selectedModel->getPhone();
            }
        }

        return new RedirectResponse('sms:'.implode(",", $phones));
    }
}
