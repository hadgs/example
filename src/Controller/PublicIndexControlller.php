<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class PublicIndexControlller extends Controller
{
    /**
     * @Route("/", name="public_index_redirect")
     *
     * @return RedirectResponse
     */
    public function index()
    {
        return new RedirectResponse($this->generateUrl('admin_app_roj_list'));
    }
}
