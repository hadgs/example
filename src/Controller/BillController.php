<?php

namespace App\Controller;

use App\DBAL\Types\BillDirectionType;
use App\DBAL\Types\BillStateType;
use App\DBAL\Types\YearlyAccountType;
use App\Entity\YearlyBill;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BillController extends CRUDController
{
    /**
     * List action.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFilterTheme());

        // NEXT_MAJOR: Remove this line and use commented line below it instead
        $template = $this->admin->getTemplate('list');
        // $template = $this->templateRegistry->getTemplate('list');

        if (!empty($datagrid->getValues()['datePaid']['value']['start'])) {
            $start = $datagrid->getValues()['datePaid']['value']['start'];
        } else {
            $start = '2018';
        }
        $date = \DateTime::createFromFormat('d.m.Y H:i:s', '01.01.'.$start.' 00:00:00');
        $yearlys = $this->admin->getRepository(YearlyBill::class)->findBy([YearlyBill::YEAR => $date]);
        $bills = $datagrid->getResults();

        $results = [
            YearlyAccountType::BANKING => [
                BillStateType::MAPPING_ACTUAL => 0,
                BillStateType::MAPPING_DISPONIBLE => 0,
                BillStateType::MAPPING_REJECTED => 0,
            ],
            YearlyAccountType::CASH => [
                BillStateType::MAPPING_ACTUAL => 0,
                BillStateType::MAPPING_DISPONIBLE => 0,
                BillStateType::MAPPING_REJECTED => 0,
            ],
        ];
        foreach ($yearlys as $yearly) {
            $results[$yearly->getAccount()][BillStateType::MAPPING_ACTUAL] += $yearly->getAmount();
        }

        foreach ($bills as $bill) {
            if ($bill->getCategory()->getDirection() === BillDirectionType::TRANSFER) {
                $results[YearlyAccountType::getMapping($bill->getAccount())][BillStateType::getMapping($bill->getState())] -= $bill->getAmount();
                $results[YearlyAccountType::getMapping($bill->getAccountTo())][BillStateType::getMapping($bill->getState())] += $bill->getAmount();

            }
            $results[YearlyAccountType::getMapping($bill->getAccount())][BillStateType::getMapping($bill->getState())] += $bill->getAmount();
        }

        $text = '';
        foreach ($results as $account => $result) {
            if ($result[BillStateType::MAPPING_DISPONIBLE] !== 0) {
                $text .= ' '.$result[BillStateType::MAPPING_ACTUAL].'('.($result[BillStateType::MAPPING_ACTUAL] + $result[BillStateType::MAPPING_DISPONIBLE]).')'.' Kč '.YearlyAccountType::getReadableValue($account).',';
            } else {
                $text .= ' '.$result[BillStateType::MAPPING_ACTUAL].' Kč '.YearlyAccountType::getReadableValue($account).',';
            }
        }
        $text = rtrim($text, ',');


        return $this->renderWithExtraParams($template, [
            'sumOf' => $text,
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ], null);
    }

    /**
     * Sets the admin form theme to form view. Used for compatibility between Symfony versions.
     */
    private function setFormTheme(FormView $formView, array $theme = null): void
    {
        $twig = $this->get('twig');

        // BC for Symfony < 3.2 where this runtime does not exists
        if (!method_exists(AppVariable::class, 'getToken')) {
            $twig->getExtension(FormExtension::class)->renderer->setTheme($formView, $theme);

            return;
        }

        // BC for Symfony < 3.4 where runtime should be TwigRenderer
        if (!method_exists(DebugCommand::class, 'getLoaderPaths')) {
            $twig->getRuntime(TwigRenderer::class)->setTheme($formView, $theme);

            return;
        }

        $twig->getRuntime(FormRenderer::class)->setTheme($formView, $theme);
    }
}
