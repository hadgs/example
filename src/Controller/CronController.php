<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CronController extends Controller
{
    /**
     * @Route("/cron", name="cron")
     */
    public function index()
    {
        $message = 'Cron '.date('d.m.Y H:i').PHP_EOL;

        $message .= $this->get('service.mailer.lunch')->execute();
        $message .= $this->get('App\Services\Mailer\FirePredictionMailerService')->execute();

        $message .= 'Cron end'.PHP_EOL;

        return new Response($message);
    }
}
