<?php

namespace App\Controller;

use App\Services\GoogleService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleController extends Controller
{
    /**
     * @var GoogleService
     */
    private $googleService;

    public function __construct(GoogleService $googleService)
    {
        $this->googleService = $googleService;
    }

    /**
     * @Route("/google/", name="google_controlller")
     *
     * @return Response
     */
    public function index()
    {
        $provider = $this->googleService->authorize();

        // If there is no previous token or it's expired.
        if ($provider->isAccessTokenExpired()) {
            if (empty($_GET['code'])) {
                $authUrl = $provider->createAuthUrl();
                return new RedirectResponse($authUrl);
            } else {
                $accessToken = $provider->fetchAccessTokenWithAuthCode($_GET['code']);
                $provider->setAccessToken($accessToken);
                $this->getUser()->setGoogleToken(json_encode($accessToken));
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->container->get('session')->getFlashBag()->add('success', 'You are logged to Google account, task will be synchronized.');
            }
        }

        return new RedirectResponse($this->generateUrl('sonata_admin_dashboard'));
    }
}
