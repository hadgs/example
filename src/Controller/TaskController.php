<?php

namespace App\Controller;

use App\Entity\Task;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TaskController extends CRUDController
{
    /**
     * Move element
     *
     * @param string $position
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function moveAction($position)
    {
        $translator = $this->get('translator');
        if (!$this->admin->isGranted('EDIT')) {
            $this->addFlash(
                'sonata_flash_error',
                $translator->trans('flash_error_no_rights_update_position')
            );
            return new RedirectResponse($this->admin->generateUrl(
                'list',
                array('filter' => $this->admin->getFilterParameters())
            ));
        }
        $correction = 0;
        switch ($position) {
            case 'up':
                $correction = -1;
                break;
            case 'down':
                $correction = +1;
                break;
        }
        /** @var Task $object */
        $object = $this->admin->getSubject();
        $object->setPositionCorrection($object->getPositionCorrection()+$correction);
        $this->admin->update($object);
        if ($this->isXmlHttpRequest()) {
            return $this->renderJson(array(
                'result' => 'ok',
                'objectId' => $this->admin->getNormalizedIdentifier($object)
            ));
        }
        $this->addFlash(
            'sonata_flash_success',
            $translator->trans('flash_success_position_updated')
        );
        return new RedirectResponse($this->admin->generateUrl(
            'list',
            array('filter' => $this->admin->getFilterParameters())
        ));
    }
}
