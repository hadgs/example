<?php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

class EventCastController extends CRUDController
{
    public function createAction()
    {
        throw $this->createNotFoundException('There is no point in doing this that way.');
    }
}
