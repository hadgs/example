<?php


namespace App\Controller;


use App\DBAL\Types\AttendanceType;
use App\Entity\EventCast;
use App\Entity\User;
use App\Repository\AttendanceRepository;
use App\Repository\EventCastRepository;
use App\Repository\EventRepository;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class RojController extends CRUDController
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var AttendanceRepository
     */
    private $attendanceRepository;
    /**
     * @var EventCastRepository
     */
    private $eventCastRepository;

    private $userRepository;

    public function __construct(EventRepository $eventRepository, AttendanceRepository $attendanceRepository, EventCastRepository $eventCastRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->attendanceRepository = $attendanceRepository;
        $this->eventCastRepository = $eventCastRepository;
    }

    public function printAction(Request $request)
    {
        $type = $request->query->get('type');
        $year = $request->query->get('year');
        $fromMonth = $request->query->get('fromMonth', 1);
        if (is_null($type)) {
            $today = new \DateTime();
            $summer = new \DateTime('March 31');
            $winter = new \DateTime('September 31');
            if ($today >= $summer && $today < $winter) {
                $type = 'publicRides';
            } else {
                $type = 'brigadeDays';
            }
        }
        $this->userRepository = $this->get('doctrine.orm.entity_manager')->getRepository(User::class);
        $date = new \DateTime();
        if (!is_null($year)) {
            $date->setDate($year, $fromMonth, 1);
        } else {
            $date->modify('-3 days');
        }
        $events = $this->eventRepository->findByDateGreaterThan($date, $type);
        $people = $this->userRepository->findBy([], [User::LASTNAME => 'ASC']);
        $casts = [];
        $attendances = [];

        foreach ($events as $event) {
            $attendances[$event->getId()] = [];
            $eventCast = $this->eventCastRepository->findBy([EventCast::EVENT => $event]);
            $text = [];
            foreach ($eventCast as $cast) {
                if ($cast->getAttendances()->isEmpty()) {
                    $text[] = $cast->getLicense()->getAbbreviation();
                }
            }
            if (!empty($text)) {
                $casts[$event->getId()] = implode(', ', $text);
            } else {
                $casts[$event->getId()] = '';
            }
            foreach ($people as $person) {
                $attendances[$event->getId()][$person->getId()] = '';
            }
        }

        $dbAttendances = $this->attendanceRepository->findAll();

        foreach ($dbAttendances as $attendance) {
            $field = '';
            if ($attendance->getAttendance() === AttendanceType::NO) {
                $field = 'xxx';
            } elseif ($attendance->getAttendance() === AttendanceType::MAYBE) {
                $field = '?';
                $field .= $this->getLunchCorrection($attendance->getLunchCorrection());
            } elseif ($attendance->getEventCasts()->isEmpty()) {
                $field = '*';
                $field .= $this->getLunchCorrection($attendance->getLunchCorrection());
            } else {
                $abr = [];
                foreach ($attendance->getEventCasts() as $eventCast) {
                    $abr[] = $eventCast->getLicense()->getAbbreviation();
                }
                $field = implode(', ', $abr);
                $field .= $this->getLunchCorrection($attendance->getLunchCorrection());
            }
            $attendances[$attendance->getEvent()->getId()][$attendance->getUser()->getId()] = $field;
        }

        return $this->render(
            'roj_print.html.twig',
            [
                'events' => $events,
                'people' => $people,
                'attendances' => $attendances,
                'casts' => $casts,
            ]
        );
    }

    public function getLunchCorrection(int $lunchCorrection): string
    {
        if ($lunchCorrection > 0) {
            return ' (+'.$lunchCorrection.')';
        }
        return '';
    }
}