<?php

namespace App\EventSubscriber;

use App\Entity\Task;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class TaskOrderSubscriber implements EventSubscriber
{
    public function postPersist(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof Task) {
            $this->commonProcess($args);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof Task) {
            $this->commonProcess($args);
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof Task) {
            $this->commonProcess($args);
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        ];
    }

    private function commonProcess(LifecycleEventArgs $args)
    {
        $em = $args->getObjectManager();
        $con = $em->getConnection();
        $con->exec(
            "update task target
            join
            (
                select id, (@rownumber := @rownumber + 1) as rownum
                from task         
                cross join (select @rownumber := 0) r
                order by if(`date_due` is null,1,0),`date_due`, `priority` DESC
            ) source on target.id = source.id    
            set position = rownum"
        );
    }
}
